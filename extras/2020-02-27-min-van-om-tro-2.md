---
layout: post
title: Min vän om tro &ndash; del två
---

Okej, alltså detta är superspännande! Det är intressant att vi
har två väldigt olika sätt att närma oss frågan kring tro, och
det verkar som att våra respektive bakgrunder har påverkat oss.
Det här blir ett försök att svara på några av trådarna i din text
och en hantering av en del associationer. Jag har också läst
klart ”Is life worth living”, den är fint skriven men jag håller
inte med om allt som står där. Slutligen har jag dragit 5-6
avsnitt av myter och mysterier, och gillar den. Som sagt tycker
jag att deras slentriandissande av naturvetenskapen kan bli
tröttsamt, men det är en intressant podd som ger förslag på andra
sätt att tänka och fundera på världen.

Så, först en kommentar avseende metafysiken. Att jag använde den
termen var inte för att ha den som synonym med religion. Däremot
ser jag metafysik som förklaringsmodeller kring hur världen är
beskaffad på ett djupare plan, och att religion blir en typ av
metafysik på det viset.

## Definitionen av Gud

Min syn på tror beror också lite på hur vi definierar begreppet
Gud. Jag tror på att universum fungerar och att kugghjulen går
runt (i brist på bättre ord). På det viset tror jag också att det
finns principer som är större än människan. Vi kan kalla det
naturen, Gud, universum, det spelar ingen större roll. Jag håller
också med om att mänskligheten har en hel del hybris kring vår
roll i världen, och att det kommer straffa sig (i första hand
genom att vi sabbar den biotop där vi bor). Personligen kan jag
känna en oerhörd vördnad inför naturen och dess krafter, vilket
är något som vi i en modern tid sällan tänker på. Däremot tror
jag att naturlagarna och de principer som styr världen är
opersonliga och inte ingriper i våra egna liv. På det sättet
knyter det an till ditt tema kring ”Gud är större”.

Jag får beredvilligt erkänna att jag delvis är en tråkig och
fyrkantig naturvetare i mitt förhållande till religionen. I mitt
fall är det heller inte brist på kunskap som hindrar. Jag har
läst ganska många religiösa texter, och ingen av dem har lyckats
övertyga mig. Det kommer sig nog också av att den känslomässiga
kontakten med en Gud eller känslan av att det finns en Gud helt
enkelt inte finns. Hur hårt jag har ansträngt mig för att tro, gå
på gudstjänster, diskutera med religiösa personer eller känna
samvaro med det gudomliga har jag inte hittat den. Jag har
snarare fått en kraftig känsla av att det inte finns en Gud, och
det är det enda jag till slut kan orientera mig efter. Även om en
stor del av mänsklighetens befolkning har haft religiösa
upplevelser och erfarenheter har jag inte haft det, och då väger
alla andras upplevelser inte jättetungt. 

Ibland kan jag bli lite irriterad på argument som angränsar ”det
är bara att tro”. På någon som har en stark känslomässig
övertygelse att Gud inte finns fungerar det ungefär lika bra som
att säga ”det är bara att låta bli att tro” till någon som är
djupt troende. I slutändan kokar det nog ner till huruvida en
känner att det finns något gudomligt där. Jag menar, jag är snart
30 och min ateism beror varken på bristande information eller
ansträngning. Hade jag haft en stark känsla av att Gud finns hade
jag varit religiös för länge sen. 

## ”Is life worth living?”

Jag börjar med min destillerade tolkning av texten för tydlighet:
Livet blir värt att leva genom optimism och hopp, i sin renaste
form sprungen ur religionen. Vetenskapen är bra men otillräcklig
för de eviga frågorna, och kan leda till desillusionering och
livsleda. Tro är svårt och kräver ett leap of faith, men klarar
vi detta kan vi finna större mening i livets oklarhet och
kalabalik.

Jag håller med om att vi psykologiskt behöver ett syfte, men att
det nödvändigtvis behöver vara religion. På samma sätt som att
religion kan vara ett ursprung till moraliska regler men inte ett
rekvisit för dessa, behöver syftet inte vara nödvändigt ursprung
ur religion. (Jag irriterar mig också lätt på författare som
använder ”religion” och ”kristendom” synonymt, men det är en
bisats i sammanhanget).

Jag gör en liten avvikelse som knyter an till ”Myter och
mysterier” här. En tydlig tråd i de avsnitt jag har lyssnat på är
”det måste finnas något mer än den fysiska världen”. Jag har
funderat på mitt eget förhållande till det övernaturliga i bred
bemärkelse, och funnit den väldigt kluven. Å ena sidan har jag
diverse gammalsvenska skrock för mig. Jag kastar spillt salt över
vänster axel, jag stampar tre gånger i marken innan jag slår ut
varmvatten i naturen och jag har alltid med mig kniv i skogen
(inte bara praktiskt, kallt stål skyddar mot oknytt). Det ger mig
en känsla av historiskt sammanhang och anknytning till naturen
när jag är ute i skogen tex, men hur mycket jag tror på det när
det kommer till kritan? Jag vet ärligt inte. Uppenbart
tillräckligt för att göra de här grejerna i alla fall. 

Genom livet har jag också haft några upplevelser som kan klassas
som spökerier och på annat sätt oförklarliga, utan att ha något
bra svar på exakt hur de har gått till. Å andra sidan är mitt
ämnesområde hjärnan, och en sak som alla källor är överens om är
att den inte är byggd för att uppfatta verkligheten korrekt. Den
är till för att planera och sortera information, men vi har
oändligt många kognitiva bias som påverkar vår uppfattning om
världen. (Här är en [kul video på temat][1].) 

[1]: https://www.youtube.com/watch?v=qpPYdMs97eE "The new ŠKODA Fabia Attention Test"

Nåväl, åter till filosofin. En existentialistisk utgångspunkt
ligger mig ganska nära, dvs att livet i sig inte har någon
inneboende mening utan att vi skapar denna själv. Detta kräver
ett stort ansvar för att inte gå vilse, vilket är varför vi måste
funderar kring de stora frågorna. Sista veckan har jag funderat
kring frågan ”Om det inte finns något mer än det vi kallar den
materiella världen, är det så illa?” Än har jag inget svar, men
för mig så känns det inte så illa ändå. 

Generellt finner jag ett stort inre lugn i att livet är kort,
döden nära, att universum är stort och vi oändligt små, samt att
vi måste skapa vår egen mening under den tid vi har. Inte för att
döden är eftersträvansvärd, utan för att livet är det enda vi
har. Det är i perioder fint och i perioder eländigt, men vi får
göra vad vi kan av det under tiden. Livet är i mina ögon
snararast värt att leva för att vi inte har några rimliga
alternativ. Döden kan för en del säkert vara en lindring men är
ju knappast bättre då den är finit och statisk. Det är bara livet
som kan ge oss en chans att förbättra världen och oss själva.
Känslan att vår tid är begränsad tvingar mig att konstant se över
hur jag vill leva mitt liv på så bra sätt som möjligt, och det
fyller mig med hopp och optimism. 

Så, det var nog det! Hoppas att det hänger ihop någorlunda, sista
redigeringen är gjord mellan två nattjourer ^^ 
