---
layout: post
title: Min vän om tro &ndash; del tre
---

Hej August!

Till att börja med, ledsen för det sena svaret! Att brevväxla har
aldrig varit en snabb sport, och den här typen av brev i
synnerhet. Det tar lite tid att tänka och organisera sina tankar,
och sen lyckas uttrycka dem på ett förståeligt sätt. Dessutom var
det lite av ett psykologiskt äventyr att fylla 30, så jag har
behövt ägna en del tankekraft åt vad som hänt där. Tack för
tålamodet hursomhelst! 

Det du säger om att pröva tankegångar stämmer nog för mig med,
och är nog en av anledningarna till att det tar en stund att
skriva de här breven. Är det så att jag missförstått vad du
skrivit i ditt senaste brev (eller om du haft en tankeprocess som
du kommit vidare i) får du säga till. Jag får dock fortfarande
känslan att vi angriper samma problem från två olika håll, eller
liknande problem med två olika metoder. Det handlar kanske minst
lika mycket som tro och gudomars existens som att hitta mening
och syfte i en kaotisk värld. Jag vet inte, men en provtanke där
också.


## Guds väsen

Jag kan förstå vad du säger när du vill att Gud ska vara något
som du kan bemöta, eller uppleva i sig. Jag kan också uppskatta
det svårbegripliga och mysteriefyllda i ett så pass större väsen.
Att jag har svårt att både känslomässigt och intellektuellt känna
en sån koppling är nog som sagt en av de största anledningarna
till att jag inte är troende.

Jag tycker tanken på en Gud som inte ingriper i våra vardagsliv
(eller våra liv över huvud taget) är en intressant tanke. På ett
sätt blir det en mer övermänsklig uppenbarelse, vars vägar är än
mer outgrundliga än vi först kan ana. Det ligger nog närmre den
bild jag har av hur Gud skulle bete sig om den existerade. Det
finns nästan något deistiskt i det. Gud som urmakare och skapare
av Universum, som sedan håller hjulen snurrande. Tar hand om sitt
verk i stort, men sköter det enligt en större och mer avancerad
plan än vad vi förstår. 

Å andra sidan tycker jag att den typen av Gud blir svårare att ha
en personlig relation med. Det landar nog återigen kring hur
något så annorlunda än oss kan vara möjligt att ens ha ett
förhållande, eller ett förhållningssätt till. Men det kanske är
som det står i Upanishaderna, att ”Gudarna älskar det fördolda
och hatar det uppenbara”. Jag är nog ganska nöjd med mina
opersonliga naturkrafter, men to each his own. 


## Tro i sig

För att återknyta till Williams så håller jag absolut med om att
religion kan vara ett rimligt svar på livets och världens
omständigheter. Jag har vänner och bekanta som beskrivit religion
som en masspsykos, och som jag tror att jag skrivit innan håller
jag med om att det är överdrivet förenklande. Folk är religiösa
eller inte av ett stort antal upplevelser och orsaker, och sådana
uttalanden blir ju föga medlidsamma. Däremot kan den hetsiga
delen av mig som vill ha absoluta och tydliga svar börja skruva
på sig frenetiskt när det börjar handla om upplevelser och
erfarenheter av gudomlighet. Den börjar komma med intensiva
invändningar om att Guds existens inte kan baseras på något så
subjektivt som personlig erfarenhet. 

Medan det jaget ångar upprört iväg mot horisonten kan min lite
lugnare sida konstatera att tro alltid kommer vara subjektivt. I
grunden handlar det kanske att om våra egna erfarenheter av Guds
(i ytterst bred och ekumenisk mening) eventuella existens lägger
grunden. Sedan använder vi diverse mer avancerade argument för
att motivera denna uppfattning, en mekanism som förekommer till
exempel med politiska åsikter. Det kan vara därför som många av
mina tankegångar och argument kokar ner till min känsla av att
Gud inte finns, och att du har flera erfarenheter som talar i
motsatt riktning.

En sidotanke, som jag får underhålla på egen hand, är vilken grad
av bevisföring som skulle krävas för att jag ska gå med på att
Gud existerar. Frågan är om jag över huvud taget skulle acceptera
att det fanns. Frågan är om jag över huvud taget vill att Gud
finns. Men det är en separat tanke som jag får underhålla på egen
hand.


## Vetenskapliga förklaringar

När det kommer till vetenskapliga förklaringar kan vi ha en hel
sidodisskussion om vetenskapsteori och hur en kan se på
verkligheten. Är det något som kan undersökas, kan vi vara säkra
på vad vi undersöker, finns det ens en yttre värld? Min
grunduppfattning att det finns en yttre materiell värld som vi
sedan uppfattar och tolkar med våra sinnen. Descartes slår till
igen så att säga. Giltigheten i det går ju att diskutera (apropå
det inre och det yttre som vi pratat om tidigare). Jag tycker det
är intressant att fundera över ett slags monism, som jag
uppfattar som ett genomgående tema i tex Myter och Mysterier. Det
går däremot på tvärs med hur jag är skolad rent
neurovetenskapligt, så det blir helt klart en krock där.

Vetenskapliga definitioner kan ju också ha svårt att vara
heltäckande eller ens grundläggande. Det finns flera saker som vi
har en intiutiv uppfattning kring, men som är oförutsett svåra
att lägga definitioner på. I mitt eget fält har vi flera
numeriska skalor för att mäta medvetandegrad, men faktum kvarstår
att det inte finns en entydig och lättfattlig definition av vad
”medvetande” innebär. Det finns flera aspekter och fenomen som
man beslutat ska ingå, men inget av dem är heltäckande. (En annan
utmärkt parallell är begreppet liv som fortfarande står utan
entydig definition. Störande men sant.)

Med detta i bagaget så förstår jag delvis kritiken i Myter och
Mysterier, kring att vissa upplevelser som inte passar in i våra
modeller sorteras ut och sållas bort. Till vetenskapens (i mycket
bred mening) försvar vill jag dock hänföra dels att den anpassar
sig till vad som observeras, vilket knappast kan sägas om vissa
trosuppfattningar. Sen kan jag också bli irriterad på bilden av
naturvetenskapen som själlös. Om vi bortser från de i mina ögon
trista statistiska delarna handlar det i grunden om att gå in
djupare i världen omkring oss och förstå vårt sammanhang. På ett
sätt är det inte helt olikt ett andligt sökande, även om svaren
är av en annan karaktär. 

Jung har jag funderat på en del, framförallt när jag läste I
Ching för ett tag sen. Tyvärr får jag nog spara det till ett
senare brev om detta någonsin ska bli klart, men onekligen en
spännande tänkare. Jag lånade Människans symboler på biblioteket
häromdagen, och hoppas att det ska bli en intressant läsning.


## Mening

Vad gäller mening i livet har jag nog använt ”finna” och ”skapa”
ganska utbytbart. Båda fungerar för mig, men jag tror att jag
ibland välja skapa för att det implicerar ett större personligt
ansvar. Men ett sökande och finnande är ju också en typ av
skapande, på sätt och vis. Jag tror att ytterligare en anledning
till att jag skriver skapa är att jag inte är säker på att det
finns en klar väg att finna, men det är en provtanke som nog
behöver tänkas vidare.   

Ingen quick fix håller jag med om. Behövs otroliga mängder
tankeverksamhet oavsett vägen man vill gå. 

Vad roligt att du tycker om det jag skrev om döden, det är något
som jag har funderat ganska mycket på. Och jag håller med,
oavsett livsåskådning finns det nog inga genvägar till att bli en
klokare och bättre person. Jag läste Pesten av Camus nyligen, som
verkligen kan rekommenderas. En av karaktärerna, som hjälper till
mycket med att bota de sjuka, får frågan varför. Han svarar att
han funderat på om det går att bli ett icke religiöst helgon och
att han därför måste hjälpa de sjuka. Det kokar på något sätt ner
till frågan om hur en kan leva ett bra liv, med eller utan
religion. Det finns en fin symbolik i det på något sätt.

Så, nu har jag funderat igenom det här brevet länge nog. Delar av
det får läsas med snälla glasögon, men nu är det klart att skicka
i alla fall!

Bästa hälsningar,  
Din vän
