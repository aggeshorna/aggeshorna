---
layout: post
title: Min vän om tro &ndash; del ett
---

SÅ. Först behövs lite bakgrund till varför jag tror som jag gör.
Jag är uppfostrad i en ateistisk/agnostisk familj, där det är ett
undantag att vara religiös. Jag kommer ju också från Jönköping,
så mitt i bibelbältet. Vi var tre pers i min åk 8 (av 150-200
pers) vilket gjorde att kristna skolgruppen skulle försöka frälsa
bort min då milda agnosticism. Det fick lätt motsatt effekt och
jag var under många år rätt sur på allt vad kristendom heter. Nu
är jag mer vuxen och har en mer nyanserad bild. Jag är dock
övertygad om att min uppfostran bidragit mer till min
livsåskådning än allt annat. 

Som det ser ut i dagsläget ser jag mig som agnostisk ateist.
Religion är metafysik, och vetenskapen kommer aldrig kunna bevisa
eller motsäga existensen av ett högre väsen (om man inte drar en
Deineken och säger att gudar egentligen är aliens, vilket jag är
lätt skeptisk till).

Rent personligt känner jag inte av någon form av gudomlig
närvaro, och jag är heller inte intresserad av evigt liv. Finns
det en gud i av den kristna sorten tror jag att den har fullt upp
med att rodda universum och inte är intresserad av mitt
vardagsliv. Jag tror dessutom att en sån gud är så väsensskild
från oss människor att den inte skulle begripa oss. 

Det gör inte att jag tycker att tro i sig är onödigt eller dumt.
Jag tycker delar av det moderna ateistcommunityt beter sig som
drygpellar när de bashar all form av religion. Jag har levt ett
ganska lätt och enkelt liv, och jag har inget intresse av att ta
ifrån folk deras hopp och längtan. Seminihilistisk existensialism
är inte för vem som helst och det är jag helt med på. Sen kan tro
vara en såväl uppbyggande som extremt destruktiv kraft. En präst
jag kände beskrev det som att eld kan vara både livräddande och
livsfarlig. Sen ska religionen inte skyllas för allt som gjorts i
dess namn. Folk hade troligt krigat och förtryckt varandra av
andra skäl om de inte haft religion (mvh lätt deppig
människosyn). Dock är religion i sämsta fall ett utmärkt verktyg
för strukturellt förtryck, vilket är trist. 

Slutligen tycker jag att kunskap kring religioner i allmänhet och
kristendom i synnerhet är nödvändig för alla som vill förstå
västerländsk idéhistoria. Jag försöker läsa de grundläggande
skrifterna i alla religioner (ett pågående arbete minst sagt).
Jag tycker dock att ateister behöver läsa bibeln mm för att kunna
föra ett vettig debatt/dialog/personlig utveckling. Hur ser du på
tro?
