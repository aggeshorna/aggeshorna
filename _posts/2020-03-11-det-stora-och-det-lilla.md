---
layout: post
title: Det stora och det lilla
---

Det stora och det lilla  
i spänningen däremellan  
lever vi våra liv  
Men kanske främst:  
Det stora i det lilla  

Att i stormens böljande tjut  
finna Gud  
Att i fågelns glada kvitter  
möta Nåden  
Att i lövets tunna ådror  
ana Livet  
Att se tiden rämna  
och ges en skymt av det eviga  

Det stora kräver det lilla  
för förankring  
för att göras verkligt  
för att inte stelna och tömmas på liv  

Det lilla kräver det stora  
för innehåll  
för sammanhang  
för mening  

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
	<img src="/images/2020-03-11-breaking-through-rochelle-blumenfeld.jpg"
	style="width:90%">
	<figcaption> 

		<i>Breaking through</i> (2013) av Rochelle Blumenfeld.

	</figcaption>
</figure>
