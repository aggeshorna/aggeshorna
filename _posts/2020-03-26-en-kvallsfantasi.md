---
layout: post
title: En kvällsfantasi
---

Jag befann mig i en skog långt under jorden. Träden var
förvridna, tysta och döda och marken var täckt av länge sedan
fallna, förstelnade löv. Det var mörkt och i handen bar jag en
lykta. Sakta tog jag mig fram igenom den täta, mörka skogen, och
i mörkret runtomkring kände jag hur mörkerbestar strök omkring
likt svultna vargar och betraktade mig med lystna ögon.

Så småningom kom jag till en glänta. Den var upplyst av ett
gråaktigt sken, som under en mulen höstdag, och i gläntan stod
ett litet, runt gravkapell. Lättad över att hitta skydd tog jag
mig raskt fram till kapellets port. Jag bankade hårt på porten
och bad om att bli insläppt, och strax gled porten, som på egen
hand, ljudlöst upp.

Innanför var kompakt mörker. Jag tvekade en stund, men vågade
till slut steget in. Med detsamma överrumplades jag av bilden och
känslan av att bli slukad av ett rovdjursgap. Jag frös till av
rädsla och obehag. Men ingenting mer hände, och jag fortsatte.

Scenen förändrades. Jag befann mig nu inne i kapellet, men i
stället för kompakt mörker var där ett vagt grönaktigt sken. Och
det var inte längre något litet gravkapell, utan ett väldigt
mausoleum. Jag stod strax innanför porten i en smal korridor, och
i valv längs med väggarna stod kistor uppradade. Korridoren var
lång, och vid dess slut korsades den av ytterligare en smal
korridor, även den med kistor i valv längs med väggarna.

Jag begav mig djupare in i mausoleumet och stötte strax på ett
vandrande skelett. Skelettet verkade sköta om gravplatsen och
dess gravar -- en död som vårdade de döda -- och det erbjöd sig
att vara min ledsagare genom mausoleumets kistprydda labyrint.

Skelettet förde mig djupare in i gravkomplexet. Mausoleumet
verkade vara konstruerat som en cirkelformad labyrint, fylld med
korridorer och återvändsgränder, men så småningom kom vi till ett
runt rum i labyrintens mitt. I rummet stod en piedestal, och på
piedestalen fanns en jadegrön, avlång sten. Stenen var inte
större än att den bekvämt kunde hållas i handen, och dess yta
var alldeles slät.

Jag kände igen stenen som de vises sten, alkemisternas *lapis
philosophorum*, nyckeln till ett evigt liv. Upptäckten gjorde mig
glad. Här i de dödas rike, bland gravar och lik, i en forna
tiders gravplats, hade jag funnit en mäktig hemlighet. En
hemlighet jag inte än förstod mig på, men ett viktigt fynd trots
allt. Men jag blev också rädd. Denna hemlighet skulle mycket väl
kunna missbrukas, och inom mig kände jag frestelsen att själv
missbruka den.

För att få veta mer om stenen började jag, med min ledsagares
hjälp, att väcka upp de döda i deras gravar. Kanske kunde någon
av dem berätta om den hemlighet jag funnit, kanske kunde någon av
dem ge mig en ledtråd till hur den kan och bör brukas. Men här
tog fantasin slut och några svar hann jag aldrig finna. I stället
befann jag mig åter i sängen i mitt sovrum.

<figure style="margin-bottom: 1.5em; margin-top: 2.5em; text-align:center"> 
	<img src="/images/2020-03-26-the-forest-max-ernst.jpg"
	style="width:100%">
	<figcaption> 

		<i>The Forest</i> (1927&ndash;28) av Max Ernst.

	</figcaption>
</figure>
