---
layout: post
title: Min syn på tro &ndash; del tre
---

*I ett pågående samtal med en vän kom vi in på temat tro och
religion. Detta är mitt tredje brev på temat, och det är ett svar
på ett [tidigare brev][brev-vän] från min vän. På bloggen finns
även [mitt första][del-ett] och [mitt andra][del-två] brev.*

[brev-vän]: /extras/2020-06-19-min-van-om-tro-3 "Min vän om tro &ndash; del tre"
[del-ett]: /min-syn-pa-tro/ "Min syn på tro &ndash; del ett"
[del-två]: /min-syn-pa-tro-2/ "Min syn på tro &ndash; del två"

Halloj kära vän,

Tack för ditt senaste brev! Jag uppskattar verkligen vårt samtal.
Det känns väldigt värdefullt att ta del av dina tankar, och att
få utveckla mina egna. Att ditt brev dröjde en smula är inget
problem alls. Att tänka måste få ta sin tid. Därav detta sena
svar tillbaks.

Min tanke var att hålla detta brev lite kortare än de tidigare,
men du ser ju hur det gick. I vilket fall som helst så hittar du
bilagd till detta brev även [en text jag skrivit][bilaga] i ett
annat sammanhang. Den behandlar delvis samma frågor som vi har
berört i vårt samtal, och jag tänker att den kan vara av
intresse.

[bilaga]: /determinism-tro-och-vetande/ "Determinism, tro och vetande"

Jag vill även nämna att jag nu officiellt blivit kristen. Det
beslutade jag mig för kort efter jag skickat mitt senaste brev
till dig, och i december hade jag en mindre ceremoni i domkyrkan
för att bekräfta beslutet. I detta brev kommer jag, likt
tidigare, utgå rätt mycket från ett kristet perspektiv, och jag
tänker att det kan vara bra att veta lite vart jag står. Men nu
till saken.

Först vill jag beröra frågan om Guds ingripande i världen. Som
jag nämnt tidigare har jag svårt att tänka mig Gud som någon som
ingriper direkt i specifika skeenden i våra liv. Men detta får
inte förväxlas med att jag tror att Gud är frånvarande, något jag
kanske inte varit tillräckligt tydlig med tidigare.

För även om jag har svårt för en direkt ingripande Gud upplever
jag ändå Gud som högst närvarande. Men jag förstår inte Guds
närvaro främst genom direkta och konkreta ingripanden i våra liv,
utan Gud är snarare närvarande som livgivande och bärande kraft.

Enligt kristen tradition är vi och allt skapat till av, genom, i
och till Gud. Gud är alltings ursprung, alltings upprätthållande
kraft och alltings mål. Gud genomsyrar hela tillvaron och är
därför alltid nära. Paulus skriver ”av honom och genom honom och
till honom är allting” (Rom 11:36), och 500-tals teologen
Dionysios Areopagita menar att:

> to praise this divinely beneficent Providence you must turn to
> all of creation. It is there at the center of everything and
> everything has it for a destiny.

Jag kan inte påstå att jag helt förstår vad det innebär – det
tvivlar jag på att någon gör – men ibland kan jag ana den sanning
som detta språk försöker uttrycka. Det är detta språk jag
föredrar och känner mig mest bekväm med när jag försöker förstå
Guds aktiva närvaro i våra och världens liv.

I kristen tradition läggs även stor vikt vid Gud som *agape*, som
utgivande och (delvis) kravlös kärlek. Det är genom Guds kärlek
och nåd – och genom Gud som kärlek – vi finns till. Det är en
kärlek och en nåd som ständigt ges ut och som vi ständigt kan ta
del av om vi lär oss att ta emot och förbli öppna.

Jag kan ibland förundras över att något så underligt och
fascinerande och osannolikt som liv ens är möjligt – eller över
att det alls finns någonting och inte endast ingenting. Att
någonting finns, att existens är möjligt, går att förstå som ett
resultat av och uttryck för Guds kärlek, av Gud som agape. Liv är
nåd, som biokemisten Anders Liljas har uttryckt saken.

Det är någonstans här mitt problem med en deistisk Gud, med Gud
som urmak­are, ligger. En deistisk Gud verkar inte vara
närvarande i sin skapelse. Om Gud är en urmakare och skapelsen
hans ur är Gud skild från och utanför sin skapelse, och skapelsen
kan i stort sett fungera skild från Gud. Även för mig verkar en
sådan Gud svår att ha en levande relation till. Men om man i
stället förstår Gud som djupt involverad, närvarande och inflätad
i sin skapelse – och skapelsen som djupt beroende av sin skapare
– finns relationen mycket närmare till hands.

I ditt brev skriver du att den del av dig ”som vill ha absoluta
och tydliga svar” inte kan förlika sig med att basera Guds
existens på någonting så subjektivt som personlig erfarenhet. Det
kan jag delvis förstå. I frågan om Guds existens finns få tydliga
och absoluta svar att få, åtminstone svar i en form vi är vana
vid. Det är ett hinder för många – och har nog varit även för
mig. Men som bekant så hindrar inte det människor, både troende
och ateister, att ge en massa tydliga och abso­luta svar i frågan
i alla fall; med det vanliga resultatet att Gud förpassas till en
trång och liten låda och blir, åtminstone för mig, både rätt
poänglös och ointressant.

Men även om tydliga och absoluta svar saknas – vilket jag numera
ser mer som en tillgång än ett hinder – finns det fortfarande
viktiga källor till kunskap utöver den rent personliga
erfarenheten. Inte minst handlar det om traditionen. I ett
kristet sammanhang är traditionen buren framförallt av bibeln och
kyrkan, men självklart finnas även mycket att hämta hos teologer
och tänkare oavsett religiös tillhörighet. Dessutom finns
dialogen: samtalet människor, trosuppfattningar och discipliner
emellan.

Varken traditionen eller dialogen kan ge svar som gäller som
vetenskapliga bevis. Men de erbjuder viktiga vägar ut ur det rent
individuella subjektet. De gör det möjligt att utvidga och
fördjupa sin förståelse och förankra den i ett bredare
sammanhang; ett sammanhang i vilket vetenskapen så klart ingår.
Tro utan vetande går lätt vilse – likt, skulle det kunna hävdas,
vetande utan tro.

Detta förringar inte vikten och värdet av den personliga
erfarenheten som en viktig utgångspunkt och grund. Men
traditionen och dialogen ger verktyg för att bredda sin
förståelse. Och även om ingen av dem kan hävdas vara objektiva
ser jag dem inte heller som rent subjektiva – de är åtminstone
*inter*subjektiva och sociala.

Men kanske är den viktigaste källan till kunskap, och även den
som är svårast att få grepp om, uppenbarelsen. Uppenbarelsen har
en framträdande roll i alla religiösa traditioner, och
uppenbarelsen är något som teologen Martin Buber (för övrigt en
otroligt rekommenderad tänkare) i hög grad likställer med mötet
med det gudomliga. Om uppenbarelsen skriver Buber i boken *Jag
och Du*:

> Mötets ögonblick är inte en ”upplevelse”, som tilldrar sig i den
> mottagliga själen och saligt avrundas där; något sker därvid med
> människan. Det är ibland som beröringen av en andedräkt, ibland
> som en brottning, i båda fallen gäller det: något sker …
> [Uppenbarelsens verklighet] är, att vi tar emot det, som vi
> tidigare inte hade, och tar emot det så, att vi vet: det har
> blivit oss givet.

Uppenbarelsen kan på många sätt likna en rent subjektiv personlig
erfarenhet, men jag tror att det är ett misstag att förminska
uppenbarelsen till blott det. Om uppenbarelsen har ett värde,
vilket jag tror att den har, måste den, i någon mån, i djupet
innebära en konfrontation med verkligheten själv. Martin Buber
igen:

> Livet inför Anletet är livet i Den Enda Verkligheten, den enda
> sant ”objektiva”. Den människa som drar ut till detta liv vill
> rädda sig in i det, som i sanning är objektivt, från det som ger
> sken därav … Subjektivism är att göra Gud till blott och bart
> själ, objektivism att göra honom till föremål; det senare betyder
> skapandet av falsk fasthet, det förra falsk befrielse, båda
> delarna ett avvikande från verklighetens väg, båda delarna försök
> att skapa en ersättning för den.

Och nu inser jag att vi delvis är tillbaks där vi började, i att
den konkreta erfarenheten av det gudomliga är en förutsättning
för en djupt förankrad tro på detsamma. Men samtidigt tror jag
att det faktiskt är på det viset, och att det därför är lönlöst
att försöka hamna någon annanstans. Men jag tror även att det är
ett misstag att hänvisa sådana erfarenheter till det rent
subjektiva. Jag blir i stället allt mer övertygad om att det är i
just sådana stunder, stunder då vi möter det gudomliga, då vi
ställs inför Anletet, som vi är i djupast kontakt med världen och
verkligheten.

Likt din så är nog min grunduppfattning att det finns en yttre
materiell värld som vi uppfattar och tolkar med våra sinnen. Som
de barn av upplysningen vi alla, åtminstone i västvärlden, i hög
grad är tror jag det är svårt att på allvar uppfatta världen på
något annat sätt. Men även jag tycker tanken om en monistisk
värld är intressant att leka med, även om min uppfattning om vad
det egentligen innebär är ganska vag. Det känns som vi behöver
hitta vägar ut från den dualistiska världssyn som vi till stor
del är fast i, och som jag förstått det är det någonting som till
viss del pågår, inte minst inom vetenskapen. Det känns hoppfullt.

Det är en viktig poäng du lyfter om det vetenskapliga sökandet.
Att det på ett sätt är ganska likt ett andligt sökande, även om
svaren är annorlunda. Det är även min uppfattning. Jag undrar om
det inte finns en likhet mellan bra vetenskap, bra filosofi och
bra teologi i att de alla ställer fler frågor än de ger svar –
och i att de alla väcker förundran genom att visa hur fantastisk
och underlig och stor tillvaron faktiskt kan vara.

På tal om att skapa och finna mening så hörde jag nyligen
kognitionsforskaren John Vervaeke prata om detta i sin
föreläsningsserie *Awakening from the meaning crisis*. Även han
har problem med begreppet finna mening. Han menar liksom du att
det inte finns någon färdigformad mening ute i världen att bara
hitta, en uppfattning jag i hög grad delar.

Men han har även svårt för begreppet skapa mening, och han menar
att ”meaning is not something we are imposing willfully on the
world”. I stället föredrar han begreppet odla mening. Han säger:

> Meaning is something between us and the world, like the way you
> culti­vate a plant. You're doing stuff with the plant but you are
> also allowing the plant to unfold. You're cultivating with the
> world, meaning between you and the world.

Spontant tycker jag det låter väldigt klokt. Vi kan odla mening
på liknande sätt som vi kan odla visdom.

Det var allt jag hade för denna gång. Men jag vill avsluta med
ett citat från Jonna Bornemarks *Det omätbaras renässans* som jag
återupptäckte för ett tag sedan. Citatet berör en del av det vi
pratat om, och jag tycker att det är rätt bra.

> Den som älskar någon vet nämligen inte allt om den älskade utan
> måste utgå från en tilltro till både sin kärlek och den älskade.
> Eftersom vår existens största frågor och grundläggande kategorier
> är inpyrda av icke-vetande kan vi inte ha kunskap om dessa utan
> bara ha en tilltro till vår egen riktning och till den förundran
> som gör världen öppen. Tro har på så sätt inte bara med det vi
> idag kallar religion att göra.

Kul förresten att även du har läst, uppskattat och funderat en
del kring Jung. Det låter som ett spännande samtalsämne att
utforska vid tillfälle. Jag hoppas att du klarar dig bra trots
allt coronakaos, och jag ser fram emot att ses igen någon gång
när det passar. Kramar!

Hjärtliga hälsningar  
vännen August

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img
  src="/images/2020-06-19-relevation-remedios-varo.jpg"
  style="width:100%">
  <figcaption> 

	<i>Revelation (The Clockmaker)</i> (1955) av Remedios Varo.

  </figcaption>
</figure>
