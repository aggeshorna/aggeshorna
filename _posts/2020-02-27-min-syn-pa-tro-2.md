---
layout: post
title: Min syn på tro &ndash; del två
---

*I ett pågående samtal med en vän kom vi in på temat tro och
religion. Detta är mitt andra brev på temat, och det är ett svar
på ett [tidigare brev][brev-vän] från min vän. På bloggen finns
även [mitt första][del-ett] och [mitt tredje][del-tre] brev.*

[brev-vän]: /extras/2020-02-27-min-van-om-tro-2 "Min vän om tro &ndash; del två"
[del-ett]: /min-syn-pa-tro "Min syn på tro &ndash; del ett"
[del-tre]: /min-syn-pa-tro-3 "Min syn på tro &ndash; del tre"

Hej svejs kära vän,

Tack för ditt svar! Väldigt intressant att få lite större inblick
i och förståelse för hur du tänker. Förhoppningsvis lyckas jag
fånga upp några av trådarna på ett vettigt sätt. 

Det bör dock sägas att det som står nedan bör tas med en eller
flera nypor salt. Mycket av det jag skriver är ett första
prövande försök att sätta sådant i ord som jag har en ganska vag
uppfattning om. Dessutom är jag rätt mycket i början av att
utforska sådant som min tro, min andlighet, min relation till Gud
och Guds möjliga väsen. Även om jag har försökt att formulera mig
så tydligt som jag för tillfället kan är det oklart i vilken mån
jag faktiskt kommer kunna stå för det jag skrivit.

Du skriver om definitionen av Gud och om att du tror på principer
som är större än människan, typ naturlagarna, men att de är
opersonliga. Just det personliga är nog en förutsättning för att
begreppet "Gud" ska kännas relevant. Min upplevelse av Gud, tror
jag i alla fall, grundar sig till stor del i att jag i världen
kan hitta något form av tilltal. Att jag kan vända mig mot
världen, universum, och hitta något jag kan benämna "du" och ha
en relation till.

Huruvida detta "du" konkret ingriper i världen är en annan fråga.
De bilder och beskrivningar av Gud som jag känner mig mest bekväm
med är de som pratar om Gud i stil med existensens fundament
eller tillvarons fundamentala verklighet. Eller som 500-tals
teologen Dionysios Areopagita skriver:

> the Life of the living, the being of beings, it is the Source
> and the Cause of all life and of all being, for out of its
> goodness it commands all things to be and it keeps them going.

Jag har svårare med tanken på en Gud som mer direkt ingriper i
människors vardagsliv, i alla fall på ett mer fysiskt plan. En
sådan Gud känns mig rätt främmande. För min del är det nog
relationen till Gud som är det centrala. I den relationen kan jag
bland annat hämta kraft, tröst, mod, glädje, kärlek och frid, och
på så sätt kan Gud vara en aktiv del av mitt liv.

Jag känner att jag här vill slänga in en liten passus om ordet
"tro". Ordet tro och uttrycket att "tro på Gud" kan vara
problematiska, åtminstone på det sätt det ofta förstås. Allt för
ofta används tro i meningen att hålla vissa lärosatser för sanna,
ofta utan någon riktig grund. Det är väl lite det tänket som
möjliggör de argument du mött som lyder ungefär "det är bara att
tro". 

I den mån jag tror på Gud är det inte främst på det sättet jag
använder begreppet. Det centrala är i stället mina faktiska
upplevelser av Gud, och i den mån jag upplever Gud vara ett
begrepp som har bäring, relevans och värde utifrån mina
erfarenheter av världen. För att citera Jung: "man måste erfara
och veta".

Det betyder inte att tro är oviktigt -- speciellt inte tro som
mer handlar om ett deltagande och ett aktivt engagemang än om att
hålla vissa föreställningar för sanna. Det finns delar av
tillvaron där vår möjlighet till vetande är begränsad, och där
kan tron vara viktig. Men tro kan vara mer eller mindre
välgrundad, och saknar tron förankring i en levd erfarenhet är
det nog lätt hänt att den både blir blind och vilseledande.

I ditt brev nämnder du även att hjärnan inte är byggd för att
uppfatta verkligheten korrekt, utan för att planera och sortera
information. Det ligger säkerligen mycket i det, jag försöker på
inget sätt avfärda vetenskapen, men när jag hör ett sådant
påstående kan jag också bli lite fundersam kring hur
"verkligheten" definieras. Min upplevelse är att en vetenskaplig
syn på verkligheten ibland kan vara lite väl snäv, och att den
därför riskerar att gå miste om värdefulla erfarenheter och
insikter.

Per Johansson nämner i något avsnitt av *Myter & Mysterier* att
vetenskapliga förklaringar ibland känns mer som bortförklaringar
än riktiga förklaringar, en upplevelse jag kan känna igen mig i.
Att det finns vissa typer av erfarenheter som naturvetenskapen, i
alla fall i den mån jag känner den, helt enkelt är oförmögen att
ta på ordentligt allvar.

För tillfället håller jag på att läsa Jungs självbiografi. Jung
är onekligen en spännande tänkare -- och ganska konstig, men det
är väl mycket av charmen. En av de saker jag finner uppfriskande
med Jung är just hans relativt inkluderande syn på verkligheten.
Upplevelser som i många andra fall skulle förkastas som
vidskeplighet eller någon form av hjärnspöken tillmäter han, om
inte annat, en psykologisk verklighet. Eftersom vi människor
fungerar som vi gör är det ett psykologiskt universum vi till
stor del bebor och verkar i. Att något har en psykologisk
verklighet är därför av stor vikt.

Angående mitt eget förhållande till det övernaturliga är också
den rätt kluvet, men kanske kluvet på ett annat sätt än för dig.
Jag är i grunden skeptisk mot de allra flesta övernaturliga
utsagor, men samtidigt finner jag ofta en allt för materialistisk
världsbild otillräcklig. Till viss del kommer det tillbaks till
det jag nämnt om tro ovan. Jag kan ganska ofta känna att det hade
varit förmätet av mig att helt förneka eller avfärda olika
övernaturliga fenomen, men för att de ska bli mer än en rent
teoretisk möjlighet måste jag kunna grunda det i egna
erfarenheter och upplevelser.

Det är väldigt vackert det du skriver om att du finner ett lugn i
livets korthet och vår egen litenhet, och att du finner hopp och
optimism i att döden tvingar dig att konstant se över hur du
lever ditt liv. Det låter som en mogen och sund inställning till
både livet och döden. Till viss del kan jag känna igen mig i det.
Mycket av det jag skrivit på bloggen är på ett eller annat sätt
en uppgörelse och en konfrontation med döden, och något av det
jag lägger vikt vid är just dödens roll som läromästare och hur
döden kan lära oss att leva.

Till rätt stor del känner jag igen mig även i din inställning om
att vi själva måste skapa vår egen mening, även om jag nog själv
skulle lägga större vikt vid att "finna" snarare än att "skapa".
Jag har i en tidigare text skrivit om att det beständiga, eller
det på riktigt värde- och meningsfulla, "finns överallt
runtomkring oss, men till stor del gömt och inte än realiserat".
Meningen är, tror jag i alla fall, någonting som behöver hittas,
eller en potential som behöver realiseras, i högre grad än något
som behöver skapas.

Filosofiska och religiösa traditioner, bland annat kristendomen,
kan vara till hjälp och agera som vägvisare i detta sökande, men
de är i sig själva inte tillräckliga. Du måste på ditt eget sätt
finna dina egna svar. Att bli kristen eller att börja tro på Gud
är således ingen "quick fix" för ett meningsfullt liv. Det kan
vara ett viktigt steg på vägen, men det är ingen slutdestination.

Lite kort om William James [*Is life worth living?*][live-life]:
Till stor del tycker jag att du sammanfattar texten bra, och jag
kan förstå och delvis instämma i din kritik. Men det jag nog
främst uppskattar med texten är William James förmåga att bemöta
den form av slentrianmässigt förkastande av all form av religion
som man ofta kan stöta på, inte minst hos "upplysta"
vetenskapsivrare. Jag tycker Williams argumenterar rätt väl för
motsatsen -- att en religiös tro kan vara en förnuftig och klok
respons på livets realiteter. Men jag håller med dig om att en
religiös tro inte är nödvändig för ett moraliskt och fullgott
liv.

[live-life]: https://archive.org/details/islifeworthlivin00jameuoft/ "Is life worth living?"

Jag tror jag tar och slutar där. Tack igen för ditt brev, och
för detta intressanta samtal. Jag tycker vi får försöka ses och
prata inom en relativt snar framtid. Text har ju sina fördelar,
men det kan också bli lite omständigt.

Till sist, ett poddtips. Jag började nyligen lyssna på serien
[*Awakening from the meaning crisis*][awakening] av John
Vervaeke. Det är egentligen en videoserie som ligger på Youtube,
men den går även att hitta som podd. Den innehåller mycket
intressant idéhistoria, och många klargörande perspektiv på hur
mening kan förstås. Dessutom är John i grunden
kognitionsforskare, så du slipper slentriandissandet av
naturvetenskapen. ;)

[awakening]: https://www.youtube.com/watch?v=54l8_ewcOlY "Awakening from the meaning crisis &ndash; episode 1"

Allt gott!

Din vän  
August

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img
	src="/images/2020-02-27-hope-pieter-bruegel-the-elder.jpg"
	style="width:100%">
	<figcaption> 

		<i>Spes (hopp)</i> av Pieter Bruegel den äldre, ca 1560.
		Inskriptionen i bildens nederkant kan översättas med:
		&rdquo;The assurance that hope gives us is most pleasant
		and most essential to an existence amid so many nearly
		insupportable woes.&rdquo;

	</figcaption>
</figure>
