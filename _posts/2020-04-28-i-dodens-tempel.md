---
layout: post
title: I dödens tempel
---

Jag begav mig ner mot sjön. Det var mörkt, natten låg tät, men
jag styrde ändå säkert mina steg. Det fanns en liten brygga vid
stranden, och vid bryggan lyste en lykta. Det var dit jag styrde.
Det låg en båt vid bryggan, en gammal eka i trä, och båten
bemannades av en färjekarl. 

Färjekarlen stod i båtens för och väntade på mig. Han var klädd i
en lång grå rock, på huvudet bar han en hatt i samma gråa färg.
Hans ansikte var helt svart, som ett mörkt hål ur vilket inget
ljus slapp ut, och jag kunde inte skönja några ansiktsdrag.
Färjekarlen sade ingenting, han väntade endast tyst på att jag
skulle kliva ned i båten. Jag tog ett djupt andetag, fattade mod,
och klev i.

Vi gav oss av. Färjekarlen rodde sakta ut båten på sjön och
satte sedan kurs mot ön i sjöns mitt. Överfarten gick långsamt.
Jag märkte snart att vi inte var ensamma på sjön. I det mörka,
kalla vattnet simmade stora sjöodjur, och jag såg deras väldiga,
ormlika kroppar krusa vattenytan. 

Det kändes långt ifrån tryggt att sitta där i den enkla ekan
medans odjuren strök kring skrovet. Men det fanns inget att göra.
Jag satt endast still och hoppades att vi snart var framme. Det
tog tid, allt för lång tid, men till slut nådde vi ön. Jag kände
båten kränga till när vi gled in mot stranden. Färjekarlen
hoppade i land och började dra båten upp på stranden, jag var i
land strax efter och hjälpte till.

Ön vi hade kommit till var inte stor, och mitt på ön låg en
kulle. En trappa av sten ledde upp för kullen och så snart båten
låg tillräckligt högt upp på stranden begav sig färjekarlen i väg
mot trappan. Jag följde efter.

Trappan var tung att klättra, den kändes längre att vandra än den
syntes vara från kullens fot. Medans vi vandrade bröt månen fram
på natthimlen, och plötsligt badade ön i dess milda sken. Jag
stannade upp och tittade mig omkring. Sjöns yta låg blank, inga
odjur syntes längre till, och lyktan lyste fortfarande borta vid
bryggan.

När jag väl kommit upp för trappan såg jag den byggnad som låg
där. Den var rund och liknade ett tempel, och vid ingången stod
färjekarlen. Jag gick dit och tänkte att han skulle gå före mig
in i templet, men han stod endast tyst kvar. Ensam gick jag in
genom den öppna porten.

Templet bestod av ett enda runt rum och genom en öppning i taket
strilade månskenet in. I en cirkel längs med väggarna stod
statyer -- de berättade en berättelse. Längs med salens vänstra
halva skildrades vägen från död till liv. Hur det som nyss dött
förmultnade, upplöstes, förgick, för att på nytt fogas samman och
bli till liv. Längs med salens högra halva skildrades det
motsatta förloppet: hur liv blir till död. Hur det nyss födda
växte upp, mognade och åldrades, för att sedan somna in och åter
bli till mull. Oändlig var statyernas berättelse. Död blir till
liv, liv blir till död, och så går det runt, om och om igen.

Jag stod vid templets port, just där livet tog slut och döden tog
vid, och på andra sidan rummet satt en mörk skepnad på en stol.
Den satt där stilla, just där det döda åter uppstod till liv,
höljd i en enkel, svart kåpa. Jag kände igen skepnaden: det var
Döden själv. Jag gick honom till mötes.

Döden satt med sitt huvud böjt, men när jag kom närmare tittade
han upp och såg mot mig. Först syntes inget annat än mörker under
kåpans svarta luva, men ju närmare jag kom desto tydligare
urskiljde jag ett svagt ljus som lyste där under. När jag så stod
inför honom såg jag det alldeles klart -- Dödens ansikte sken. 

Det sken med ett ljus av avlägsna stjärnor, det glittrade likt
månens blänk i vattnet. Att skåda det ansiktet var att ana
evigheten, att ges en glimt av den stora friden. Inför detta
ansikte stod jag mållös, och tyst var även Döden. Inte ett ord,
inte en viskning, krusade hans läppar; han var så tyst som när
han utför sitt värv. För döden kommer när han vill och tar vad
han önskar, och inga ord, varken till tröst eller förklaring,
lämnar han efter sig.

Men det finns språk som inte kräver några ord. Språk så ömtåliga
och så fyllda av sanning att varje ord är ett hinder. Det var med
ett sådant språk som ansiktet inför mig, med sitt stilla,
avlägsna sken, talade. Det talade om livet, det talade om döden.
Det lärde mig om hur jag bör leva, och om hur jag bör dö. Det
talade om de rymder och den vila som ryms bortom livets rand, och
om den uppståndelse och återkomst som döden gömmer. Om allt
detta, och ännu mer, talade ansiktet till mig.

Vi stod så länge, Döden tyst talandes och jag tyst lyssnandes,
och så småningom led natten mot sitt slut. Dagen grydde, och i
takt med att månens milda ljus förbyttes i dagens skärpa såg jag
hur ansiktet framför mig sakta tynade. Det sken inte längre
starkt nog, och snart gömdes det helt i kåpans skugga. Det som
syntes mig så klart i nattens mörker doldes nu av dagens allt för
klara ljus. Döden var mig åter förborgad -- utan ord, och nu även
utan språk. Jag bugade lätt, vände mig om och gick.

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img src="/images/2020-04-28-bone-field-zdzislaw-beksinski.jpg"
  style="width:95%">
  <figcaption> 

  	Målning av Zdzisław Beksiński.

  </figcaption>
</figure>
