---
layout: post
title: Ansvaret vi alla bär
---

Alla bär vi på ett ansvar. Ett ansvar för oss själva, ett ansvar
för varandra, ett ansvar för skapelsen. Exakt vart ansvaret
kommer ifrån, eller hur omfattande det är, vet jag inte. Men att
det finns där, i någon mån, är jag övertygad om.

Ansvaret tar sig uttryck på olika sätt. Ett uttryck är de
mänskliga rättigheterna. En rättighet är blott tomma ord om den
inte motsvaras av ett ansvar. Om jag har rättigheten till liv,
har du ett ansvar för att inte beröva mig detta liv. Har jag
rätten att uttrycka min åsikt, har du ett ansvar för att inte
tysta mig.

Ett annat uttryck är synden. Vi misslyckas ständigt med att leva
upp till det ansvar som är oss givet -- däri ligger synden.

I det nya testamentet är det vanligaste ordet för synd grekiskans
*hamartia*. Hamartia betyder att missa målet, likt en bågman
missar piltavlan. I det gamla testamentet används främst
hebreiskans *chatta'th*, vars innebörd liknar grekiskans
hamartia. Att synda är att missa målet. Det är att misslyckas med
att göra det rätta, att brista i ansvar. 

Svenskans *synd* har ett något annorlunda ursprung. Det härstammar
ur tyskans *sünde* viket betyder sund (som i substantivet "ett
sund", inte adjektivet "en sund person"). Ett sund är en
avskiljare, något som skiljer en landmassa från en annan. Likaså
är synden en avskiljare, något som skiljer människan från Gud.

Gud kan vara många olika saker och förstås på många olika sätt.
Ett sätt är att, likt [Dionysios Areopagiten][dionysios], förstå
Gud som Varandet, Godheten och Livet självt, som "the Life of the
living", "the being of beings" och "the inexpressible Good".

[dionysios]: http://www.aggeshorna.se/de-gudomliga-namnen/ "Guds offatbarhet &ndash; och hans heliga namn"

Att närma sig Gud är, med en sådan förståelse, att närma sig själva
källan till allt liv, allt varande och all godhet. Det är denna
källa som synden avskiljer oss ifrån. Vill vi det goda (vilket
vi, i regel, vill); strävar vi efter att främja liv (vilket vi, i
regel, gör); är det Gud vi vill, Gud som vi strävar mot. Gud är
måltavlan som vi i synden missar.

Kanske är en aspekt av vad Gud är just en personifiering av det
ansvar vi bär på. Om det jag skrivit ovan stämmer, om att synda
både innebär att brista i vårt ansvar och att "missa Gud", är det
ingen orilmlig tanke. 

Gud är alltså måltavlan vi siktar mot, men att sikta rätt är
ingen enkel sak. Jesus säger: "Var fullkomliga, såsom er fader i
himlen är fullkomlig" (Matt 5:48). Det är mot fullkomligheten vi
ska sträva, men vi är inte fullkomliga varelser. Vi är fallna och
missar ständigt målet -- syndar ständigt -- på ett eller annat sätt.
Vi envisas med att göra det vi vet är fel. Vi avstår från att
göra det vi vet är rätt. Och vi misslyckas med att göra det rätta
till och med när vi försöker.


## Nådens helande kraft

Då vi alla syndar är vi alla i behov av nåden och förlåtelsen.
Att leva är, till viss del, att misslyckas -- och att misslyckas
är tungt. 

I somras pratade jag med en vän som mådde dåligt. Hen hade haft
en särskilt tung natt och morgonen efter talades vi vid. När jag
frågade henom vad det var som kändes så tungt kunde hen inte
svara. Min väns ångest var som ett kaosartat formlöst mörker,
omöjligt att få grepp om -- och därför även omöjligt att bekämpa.
Så jag pressade min vän på frågan i hopp om att göra mörkret mer
konkret, mer hanterbart. Istället kom ångesten tillbaka och min
vän var tvungen att springa till toaletten och spy.

Jag hade misslyckats. Jag hade försökt hjälpa min vän när hen
mådde dåligt men istället fått henom att må ännu sämre. Det
kändes tungt. Jag var inte fullkomlig nog för att vara det stöd
min vän behövde.

När en har ansträngt sig för att göra det en tror är rätt, men
istället bara förvärrat situationen, är det lätt hänt att vilja
ge upp och avsäga sig sitt ansvar. Men det är -- åtminstone i de
flesta fall -- fel sak att göra. En av Jordan B. Petersons tolv
livsregler lyder: "Do what is meaningful, not what is expedient."
Att ge upp och avsäga sig sitt ansvar är ofta "expedient",
egennyttigt eller bekvämt, men det är sällan det meningsfulla --
eller det rätta.

I sådana stunder kan nåden, och den förlåtelse den ger upphov
till, vara till hjälp. Nåden och förlåtelsen som en förvissning
om att det är okej att misslyckas, att inte vara fullkomlig. Utan
förlåtelsen kan ansvaret, och de misslyckanden som ofta följer i
dess spår, väga allt för tungt.

Alternativet till förlåtelsen är domen. Att misslyckas när ingen
förlåtelse finns att få är att vara dömd -- och vem orkar axla
sitt ansvar om allt som väntar är domen? Förlåtelsen kan ge oss
kraft nog att axla det ansvar som är oss givet, och få oss på
fötter igen efter vi har fallit.


<figure style="margin-bottom: 1.5em; margin-top: 2em;">
	<img src="/images/2019-04-09-law-and-grace-lucas-cranach.jpg"
	style="width:100%">
	<figcaption> 
	
		<i>Lag och nåd</i> (1529) av Lucas Cranach den äldre.

	</figcaption>
</figure>

