---
layout: post
title: Scener från ett slagfält
---

Mannen blickade ut över slagfältet och kände endast sorg. Sargade
kroppar täckte marken, lukten av död och förruttnelse låg tät. Så
mycket lidande. Så mycket idioti. Så många liv utsläckta i
förtid. Vid tillfällen som dessa fann han ingen glädje i sin
uppgift. Men han hade sin roll att spela, sin plikt att utföra;
det hjälpte inte att dröja.

Han böjde sig ner vid pojken, knappt äldre än ett barn, som låg
vid hans fötter och lade handen på pojkens axel. Pojken öppnade
ögonen. Han satte sig upp och tittade förvirrat omkring. Pojken
sade ingenting. Mannen tog till orda:

"Kom, det är dags. Din tid är kommen och det är dags att fara."

Pojken reagerade inte på mannens tilltal. Han fortsatte endast
att se sig förvirrat omkring, oförmögen att ta till sig det
sceneri som omgav honom, den situation han befann sig i.

Mannen kände igen beteendet. Han hade besökt många, oerhört
många, i sina dagar och han kände väl igen de allra flesta
reaktioner vid det här laget. Det var inget särskilt att göra.
Pojken skulle komma till insikt så småningom. Det fanns inget
annat val. Alla var de tvungna att acceptera sin lott, resignera
inför sitt öde, och ge sig av. Mannen gick vidare.

<hr class="textSeparator">

Han knäböjde vid de lemlästade, vidrörde de brända, fortfarande
rykande, kropparna och uppväckte de sargade, de söndertrasade, de
skjutna och de slagna. Till alla bar han samma budskap: Din tid
är kommen, det är dags att ge sig av.

Somliga for villigt. Somliga greps av vrede. Somliga greps av
skräck och somliga mest av förvirring. Vissa grät av sorg, andra
grät av lättnad, några hade redan gråtit nog för att inte ha
några tårar kvar. Somliga hade livet så kört slut på att de
endast utmattat och likgiltigt följde de anvisningar som gavs.

Pojken följde efter mannen medan han arbetade. För förvirrad för
att ge sig av stapplade han sakta efter i mannens fotspår,
storögt tittandes på slagfältet runtomkring. Mumlandes tyst för
sig själv upprepade han gång på gång samma fras: "Jag är ju inte
färdig."

Mannen kände med honom: pojken var inte färdig. Allt för ung hade
han ryckts undan från livet, och till vilken nytta? Men sådan är
världens gång. Somliga dör för unga, andra allt för gamla. Åt
detta finns inget att göra, inte ens för mannen själv. Det är
endast att finna sig i den tid som uppmätts åt en, att streta
emot tjänar ingenting till. Tvärtom: Det kan hindra en från att
ta vara på den tid man fått.

"Jag är ju inte färdig." 

Pojken fortsatte att mumla för sig själv, men den här gången
valde mannen att besvara honom.

"Och det kommer du aldrig att bli."

Pojken hajade till när han hörde mannens röst, han verkade först
nu inse att han hade sällskap. Han betraktade mannen som stod
framför honom, och mannen tittade tillbaka på pojken. Till sist tog
pojken återigen till orda, något högre denna gång.

"Det är så mycket jag inte hunnit. Så många saker jag inte gjort,
så många människor jag inte mött, så många tankar jag inte tänkt.
Så mycket liv jag inte levt."

Mannen lade ömt sin hand på pojkens axel.

"Det stämmer, och du gör rätt i att sörja det liv du mister. Men
glöm inte att vara tacksam för det liv du ändå fått, allt det liv
du trots allt levt. Du har gjort mycket. Du har sett berg och
skogar. Du har sjungit. Du har dansat. Du har delat
din glädje och din sorg med människor du älskar. Du har 
känt vinden smeka din hud; känt solens värme och vattnets
svalka. Du har tagit del av livet, och sett dess skönhet. Allt
detta har du upplevt. Allt detta har hänt. Det kan varken jag,
eller någon annan, ta ifrån dig. Mer än så kan ingen kräva."

Pojken sänkte blicken, och mannen såg att han grät. De stod så en
stund -- pojken tyst gråtandes, mannen med en hand på pojkens axel
-- innan pojken återigen lyfte huvudet.

"Du har rätt. Det måste få vara nog." Pojken suckade tungt.
"Tack." 

Pojken gav mannen en sista blick, vände sig sedan om och gav sig
av.

<hr class="textSeparator">

Mannen fortsatte med sin uppgift, fortsatte att vandra från lik
till lik, och fick så småningom syn på en annan man som likt
honom vandrade på slagfältet. Mannen hade sett honom förr, på
sätt och vis var de lite som följeslagare. Inte minst hade de
många gånger setts på platser likt denna. 

Till slut upptäckte även den andre mannen att han hade sällskap
på slagfältet. De två männen vinkade och gick varandra till
mötes.

"Jag undrade just när jag skulle få syn på dig!", hälsade den
andre mannen. Han spanade ut över slagfältet. "Jag förstår att du
har mycket att göra."

"Det har jag", fick han till svar, "och det är väl delvis dig
jag har att tacka för det."

"Jo, på sätt och vis."

De stod tysta en stund och betraktade förödelsen. Den andre
var en man med många namn: Anklagaren. Frestaren.
Motståndaren. Denna världens furste. Satan. Allt detta visste den
förste mannen, och han blev ibland fundersam kring hur den andre,
Djävulen själv, förhöll sig till sitt verk.

"Är du nöjd med vad du ser?" frågade han.

"Jovars, förstörelsen är nästan värre än jag förväntat mig."

De två männen var återigen tysta ett slag.

"Ibland undrar jag om det skall vara nödvändigt, allt detta
lidande", den förste mannen svepte med handen över
slagfältet. "Vad allt detta tjänar till."

Satan suckade. Han såg trött ut.

"Du vet, lika väl som jag, att detta egentligen inte är mitt verk
-- detta har människorna gjort mot varandra. Jag lägger ut
frestelserna och ställer upp prövningarna. Hur människorna sen
hanterar dem är deras eget ansvar."

"Du har rätt, så klart. Men ibland undrar jag om de verkligen
behöver prövas så hårt. Har de verkligen förtjänat det?"

Satan svarade efter en stunds tystnad.

"Alla har vi vår roll. Du har din, liksom jag har min. Min är
rollen som Åklagaren, Frestaren och Prövaren; så åklaga, fresta
och pröva är vad jag gör. Dessutom, vad skulle människornas liv
vara utan prövningarna och lidandet? Utan någonting att ta spjärn
och kämpa emot? Det enda jag själv vet är att det hade varit
någonting helt annorlunda, och jag tvivlar på att det hade varit
bättre för dem."

"Ja, du har väl rätt även i det." Den förste mannen såg fundersam
ut, men fortsatte sedan. "Det är ju ofta som även jag betraktats
med oblida ögon, så jag har väl egentligen ingen rätt att
ifrågasätta dig. Lev och låt leva, och allt det där."

Satan skrattade kort åt det valda uttrycket och utbrast:

"Härligt ändå att inte ens själva Döden helt förlorat sitt sinne
för humor!"

<hr class="textSeparator">

De två männen skiljdes åt. Den ena för att fortsätta med sin
uppgift, den andre för att betrakta den förödelse han själv varit
med om att orsaka.

När han vandrade mellan liken fortsatte mannen att tänka på det
samtal han just haft. "Det har sagts", tänkte han, "att det är synd
om människorna. Att de inte vet vad de gör." Han tittade omkring
sig. "Och sant är väl det. En plats som denna är bevis nog."

Men trots det kunde han inte undvika att känna en viss avund. För
det var så som Djävulen sagt: människorna har ett ansvar; ett
ansvar grundat i ett val, i en möjlighet att handla annorlunda.
Och med det valet kommer ett hopp. Ett hopp om att en gång bryta
ondskans bojor och bli något mer, något större. "För egen del",
tänkte Döden buttert medan han fortsatte med sin uppgift, "är
jag dömd att för evigt göra just detta: städa upp efter andras
idioti."

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img src="/images/2019-10-27-after-the-battle-paul-nash.jpg"
	style="width:100%">
	<figcaption> 

		<i>After the Battle</i> (1918) av Paul Nash.	

	</figcaption>
</figure>
