---
layout: post
title: En annan jord
---

Stjärnorna sken,  
vinden ven,  
och vattnets porlande brus  
blandades  
med trädtopparnas vajande sus.  
Det var som om världen  
vyssjade mig  
och sade:  

"Känn ingen oro,  
känn ingen ängslan,  
allt blir bra till slut.  
Vad ni än må göra  
kan ni aldrig förgöra  
oss.  
Hur hårt ni än må slå  
finns det alltid något  
ni aldrig kommer kunna nå.  
Ni kommer brista   
innan  
oss."  

Och stjärnornas sken  
nådde mig   
genom galaxer och ljusår  
och jag insåg  
att oavsett hur mycket  
vi skövlar och skändar,  
bränner och stympar,  
förorenar, förnedrar, förstör,  
så är det ur stjärnornas synpunkt  
likgiltigt  
(eller åtminstone nästintill).  

Och jag insåg  
att vattnets brus  
och trädtopparnas sus  
och vindens vinande över vidderna  
kommer finnas kvar  
även om vi  
är borta.  

Det gav mig tröst.  

För även om det vi har,  
och riskerar att förlora,  
är vackert och viktigt och värdefullt  
är vår makt att förgöra   
begränsad.  
Och även om vi inte lyckas  
värna den jord   
vi ärvt, kommer  
en annan jord  
leva vidare.  

<figure style="margin-bottom: 1.5em; margin-top: 2em;
text-align:center"> 
	<img src="/images/2020-02-13-en-annan-jord.jpg"
	style="width:100%">
</figure>
