---
layout: post
title: Värdet av att bara vara
---

Under stilla veckan, den vecka som leder upp till påsk, var jag
ensam på tur i Tandövala -- ett vackert och vidsträckt
naturreservat i trakterna mellan Malung och Sälen. Turen hade jag
döpt till tråkturen och planen var att under en veckas tid göra
så lite som möjligt. Jag ville öva mig i att ha tråkigt, eller
snarare i att "bara vara", för att bättre kunna få syn på sådant
som lätt tappas bort i bruset, skapa rymd för det som vardagen
tränger undan. Väl på plats boade jag in mig i en slogbod, en
form av vindskydd vanligt i Dalarna, vid en vacker liten tjärn
och gjorde sedan inte mycket mer än att elda, äta, sova, be och
att ligga på ställen och glo. Det var en bra vecka.

Medan jag låg där i skogen och inget gjorde hade jag gott om tid
att se på skogens alla syner och lyssna till skogens alla ljud.
Jag såg gryningssolens mjuka sken sakta krypa ner för snötyngda
höjder, jag såg furan som stolt och rak vajade i vinden. Jag såg
molnens raska marsch över klarblå himmel och kvällens brandröda
dis över granklädda klittar. Jag hörde orrarnas kuttrande,
bubblande spel i morgontimmen, jag hörde vattnets upprymda sorl i
avsmältningsbäcken. Jag hörde den istäckta tjärnen stöna och
stånka när förmiddagssolen värmde och jag hörde kvällsbrisens
vilda lek i trädkronorna.

Jag såg, jag hörde, och jag lät tankarna gå. Jag släppte dem på
bete i vida hagar och lät dem ströva fritt längs vindlande stigar
-- och så småningom började jag inse storheten i att inget göra.
Storheten i att lossa greppet, låta egot vila, lämna över rodret
för en stund. Storheten i att, för tillfället, glömma allt det
jag vill och borde göra och bara låta saker vara. Men jag såg
också vår, och min, oförmåga att göra just detta. 

För vi är dåliga på att inget göra. Det är som att vi ständigt
måste trixa, fixa, ändra och rätta till, som att vi ständigt, i
nån form av frenetisk rastlöshet, måste tvinga oss själva på
världen. Vi släpper inte greppet, och stänger oss därför för
världen. Världen talar, men vi är allt för insnärjda i vårt eget
för att på allvar kunna se, lyssna, ta in. I stället beter vi oss
som om världen inte duger, inte är bra nog, och som om vi inte
duger om vi inte ständigt pysslar, sysslar, donar och planerar --
om vi inte ständigt *gör* något, nästan lite vad som helst. Men
det där stämmer ju inte.

En insikt som sjunker in allt mer desto mindre jag gör är att
världen inte bara duger, den är faktiskt helt fantastisk, just så
som den är. Och på något underligt sätt som jag inte helt förstår
är det som att ju mer jag lär mig att ta in och förstå världens
skönhet och storhet, desto större blir även jag som får vara en
del av denna sköna och storslagna värld. Det är som att ju mer
jag lär mig att älska världen, desto mer älskar också världen
mig. 

Det börjar gå upp för mig att mycket av det som ändå är fel och
fult och dumt här i världen kommer just ur vår egen rastlöshet,
vårt eget självpåtvingande på allt runtomkring oss. Vi tillåter
oss inte att se och lära känna världen på dess egna villkor utan
tvingar i stället på den, och varandra, våra egna bristfälliga
idéer om hur saker är och bör vara. Vi förgriper oss ständigt på
världen -- och, i förlängning, också på oss själva.


<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img
  src="/images/2021-05-27-vid-slogboden-jana-eriksson.jpg"
  style="width:100%">
  <figcaption>

	Jag vid slogboden. Bilden är tagen under en tidigare tur till
	Tandövala i september förra året. Foto: Jana Eriksson.

  </figcaption>
</figure>

