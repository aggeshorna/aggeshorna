---
layout: post
title: Stunder av tillräcklighet
---

*Detta är den tredje och avslutande delen i en serie om att lära
sig att dö. Följ länkarna för [del ett][del1] och [del
två][del2].*

[del1]: /att-lara-sig-att-do/ "Att lära sig att dö &ndash; en introduktion"
[del2]: /att-mota-doden/ "Att möta döden &ndash; och hur det kan lära dig att leva"

Den senaste tiden har jag skrivit mycket om döden: om att lära
sig att dö, om att med hjälp av döden finna vägar som leder till
livet, om att vi genom att släppa vår fruktan för döden på allvar
kan våga leva.

Döden kommer, förr eller senare, till oss alla. Och döden kommer
när den själv behagar -- inte när det passar oss. Därför är det
klokt att redan nu göra sig redo för dens ankomst, att inte vänta
med förberedelserna. Att redan nu se till att ha någonting tryggt
till hands som inte döden kommer åt, någonting fast och hållbart
som i dödens närhet ger dig mod att säga: "Jag räds dig inte."

Döden kan vara hård och skoningslös. Mycket av det vi värdesätter
i livet blir i dödens obevekliga blick till ett intet; det smulas
sönder och förgås, och det som tidigare syntes oss viktigt och
angeläget blir plötsligt tomt och meningslöst.

Lev ett liv fyllt av sådana obeständiga ting, och tankar på döden
blir dig en plåga och lämnar dig ingen ro. Men fyll ditt liv, så
gott du kan, med saker av mer bestående värde och döden kan bli,
om inte annat, så åtminstone hanterlig. Du får ett försvar,
någonting att inför dödens visa upp, peka på och säga: "Se, jag
har inte levt förgäves."

Men vad har bestående värde? Vad finns som döden inte kan nå?
Därom tvista de lärda, och svaret på den frågan har genom tidens
gång varierat beroende på plats, tid, person och sammanhang.
Några absoluta och för evigt giltiga svar på en sådan fråga går
nog inte att få. 

Jag tror i stället att det beständiga finns överallt runtomkring
oss, men till stor del gömt och inte än realiserat. Varje
människa måste själv upptäcka, och ibland skapa, det beständiga.
Men det går, med hjälp av rätt kunskap och rätt blick, att i det
som för andra är ömtåligt och förgängligt finna någonting
beständigt -- och kanske till och med evigt.

Vad som betraktas som beständigt kommer därför skilja sig åt från
person till person. Men för mig har en av nycklarna till att
fundera kring detta varit dessa ord av Dan Andersson:

> Jag hör på stjärnorna och träden och hela den höstmörka jorden,
> på hela det stormande och underbara livet, men högst av allt
> hör jag kärlekens sång, full av gudomlig glädje mitt i all
> mänsklig sorg. Och när den sången ljuder fulltonig, då är
> lidandet en lisa och själva döden har ingen makt mer.

Det finns stunder i livet som i sig själva är fullt tillräckliga.
Stunder där ingenting väsentligt saknas, som inte är behov av
något förflutet eller någon framtid för att rättfärdiga sig
själva, som räcker till just sådana som de är. Stunder där
kärlekens sång ljuder fulltonig.

Att de stunderna har hänt, att de stunderna har upplevts, kan
inte någon, inte ens döden, förändra. De stunderna kan
ifrågasättas, de kan glömmas bort -- men de har fortfarande
inträffat, de har fortfarande upplevts. Däri äger de en
beständighet, och insikten om denna beständighet kan bli till ett
försvar.

För mig har dansen blivit ett sätt att hitta sådana stunder av
tillräcklighet, och de tankar som denna text baseras på föregicks
just av en sådan danskväll. Förra nyåret spenderade jag på The
Snowball, en swingdansfestival i Stockholm, och där --
tillsammans med vänner, med fyllda dansgolv och livemusik --
kändes livet komplett. 

På den platsen och under den kvällen ljöd kärlekens sång för mig
fulltonig, och jag kände mig innesluten och delaktig i den
sången. Utan ansträngning kom, som en gåva, melodin och orden
till mig och jag sjöng med.

Den kvällen var inte beroende av något som tidigare hänt eller
något som skulle komma att hända för att vara värdefull. Allt jag
behövde fanns just där och då. Kärlekens sång ljöd fulltonig,
och ingenting saknades. Den kvällen var tillräcklig alldeles i
sig själv.

Jag har tidigare skrivit om [ett möte med döden][möte-döden]. Om
när döden kommer, tar en på axeln och säger: "Det är dags." I den
texten söker jag just ett försvar mot döden. Någonting fast och
hållbart att peka på och visa upp, och samtidigt något som ger
oss kraften att släppa taget, acceptera att det liv vi givits nu
kommit till ett slut och villigt följa döden på färden.

[möte-döden]: /ett-mote-med-doden/ "Ett möte med döden"

Det försvar jag i den texten fann var just dessa stunder av
tillräcklighet. De stunder som inte behöver något utöver sig
själva för att finna mening, stunder som är våra och som inte ens
döden kan beröva oss. Eller som döden själv uttrycker det:

> Så, sörj det liv du mister, men fäll främst tårar av
> tacksamhet. För all den glädje du ändå upplevt. För all den
> kärlek du ändå fått och gett. För allt det liv du ändå levt. Du
> har fått se berg och skogar. Du har fått skriva. Du har fått
> sjunga. Du har fått dansa. Du har fått dela din glädje och din
> sorg med människor du älskar. Du har fått känna vinden smeka
> din hud; fått känna solens värme och vattnets svalka. Du har
> fått möta Gud. Du har tagit del av livet, och sett dess
> skönhet. Allt detta har du upplevt. Allt detta har hänt. Det
> kan varken jag, eller någon annan, ta ifrån dig.

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img src="/images/2019-08-31-encircled-flight-rochelle-blumenfeld.jpg"
	style="width:100%">
	<figcaption> 

		<i>Encircled flight</i> av Rochelle Blumenfeld.

	</figcaption>
</figure>
