---
layout: post
title: Ansvaret
---

Ansvaret bär vi alla:  
för oss själva,  
för varandra,  
för skapelsen.  

Nåden får vi alla  
den ger oss kraft att bära.

<figure style="margin-bottom: 1.5em; margin-top: 2em;">
	<img src="/images/2019-04-09-law-and-grace-lucas-cranach.jpg" style="width:100%">
	<figcaption> 
	
		<i>Lag och nåd</i> (1529) av Lucas Cranach den äldre.

	</figcaption>
</figure>
