---
layout: post
title: Determinism, tro och vetande
---

För ett tag sedan diskuterade jag determinism med en vän, och det
visade sig att min vän inte tror på den fria viljan. Hen menar
att våra tankar och handlingar helt dikteras av de naturlagar som
styr universum och att vårt eget inflytande är ytterst litet.

Det är en uppfattning jag har svårt för. Den fria viljan är
uppenbarligen begränsad, men att den inte alls existerar köper
jag inte. Påståendet om den fria viljans icke-existens går dels
att bestrida på vetenskaplig basis. Inte minst för att vi, om jag
förstått det rätt, vet alldeles för lite om vårt eget psyke för
att förkasta något så fundamentalt som den fria viljan. 

Men mina invändningar mot determinismen står inte främst på
vetenskaplig grund, utan kommer ur min upplevelse av vad det är
att vara människa. Jag upplever att jag har en, till viss del,
fri vilja. Jag upplever att jag genom mina val och handlingar kan
påverka mitt och andras liv, och jag upplever att denna möjlighet
är en förutsättning för att skapa någon form av mening i mitt
liv.

Men nog om det. Denna text är inte främst en utläggning om den
fria viljans vara eller icke vara, utan snarare en text om vad
det innebär att veta något. För även om min vän säger att hen
inte tror på den fria viljan, agerar hen som om hen gjorde det.
Hen agerar som om hen har förmågan att fatta självständiga beslut
och som om hen genom sina beslut kan påverka sitt och andras liv.
Det är inget att förvånas över, det är det enda rimliga sättet
att leva som människa. Något min vän instämmer i.

Det är här min väns och min meningsskiljaktighet blir på allvar
intressant. För här slutar den handla om att endast vara för
eller emot något abstrakt koncept -- i stället börjar den
handla om vår syn på kunskap.

Anledningen till att min vän inte tror på den fria viljan är, som
jag förstått det, att hen har presenterats med vissa fakta om hur
världen fungerar. Hen har insett att den fria viljan så som hen
förstår den inte passar med dessa fakta och därför dragit
slutsatsen att den fria viljan är en illusion. Att denna slutsats
till stor del går stick i stäv med hur hen faktiskt lever sitt
liv, och att det är en uppfattning det är omöjligt att leva efter
i världen, är för henom inte särskilt relevant.

För mig framstår denna syn på kunskap som alltför
intellektualiserad. Det är en syn på kunskap som, förenklat
uttryckt, menar att riktig kunskap endast kan nås av ett
rationellt intellekt genom värderandet av objektiva fakta. 

Att använda sitt intellekt för att värdera fakta är självklart
viktigt för att nå kunskap om världen och varandet. Men det kan
bli alltför enkelspårigt. Förutom den kritik som går att rikta
mot begrepp som rationell och objektiv upplever jag också en
brist på mänsklighet.

Som människa är jag inte ett fritt flytande intellekt. Jag är i
stället en varelse djupt involverad i en kroppslig, social och
emotionell existens. För att kunskap ska vara på allvar värdefull
för mig kan den inte endast bestå av en teoretisk hypotes att
intellektuellt omfamna eller förkasta. Kunskapen måste även vara
förankrad i den existens jag är involverad i, och ett verktyg att
bättre hantera densamma. Den måste vara något som jag inte endast
kan erfara och förankra intellektuellt, utan även kroppsligt,
socialt och emotionellt. 

Den syn på kunskap som min vän ger uttryck för saknar dessa
aspekter. Den ställer sig i stället vid sidan av och påstår sig
komma med ett objektivt, och därav mer riktigt,
utifrånperspektiv. Men genom detta vid-sidan-ställande förloras
någonting viktigt; kunskapen blir halv och ofullständig. Verklig
kunskap måste upplevas och erfaras. Vi måste, i Hagar Olssons
ord, "uppleva den, ta den till oss som vår innersta egendom, ge
den liv av vårt liv".

Jag tror att denna överintellektuella kunskapssyn är en viktig
anledning till den religiösa förvirring jag upplever som vanlig.
Många verkar tro att religionens kärna är en uppsättning
teoretiska hypoteser, både dåligt underbyggda och icke
verifierbara, att antingen tro på eller förkasta. Sett på det
sättet är religion uppenbarligen absurt. Men det är en
missuppfattning. 

I sin föreläsningsserie *Awakening from the meaning crisis*
pratar kognitionsforskaren John Vervaeke om olika sätt att veta.
I vårt moderna samhälle är vi väldigt upptagna av ett av dessa
sätt: att veta är att ha en viss typ av tro eller övertygelse ("a
special kind of belief"). Till detta sätt att veta hör mycket av
vår vetenskapliga förståelse och våra ideologiska övertygelser.
Även min väns tro på determinismen verkar till stor del hänga
ihop med detta sätt att veta.

Men det finns andra sätt att veta som till stor del försvunnit
från vår kulturella radar. Vervaeke pratar om ett vetande som
handlar om hur man fångar en baseball; ett vetande som handlar om
hur det är att ha just den här upplevelsen just nu; och ett
vetande som handlar om hur det är att delta och vara involverad i
något, exempelvis en relation.

Om vetande som en tro eller övertygelse är den enda form av
vetande som på allvar erkänns blir religion lätt absurt. Tro och
övertygelse är självklart viktiga, men det finns även andra
former av vetande som är centrala för religiös kunskap och
praktik. Vetande som i högre grad kräver närvaro och deltagande,
och som inte minst kan nås genom bön, meditation och ritual. För
religionens kärna handlar inte så mycket om ett försanthållande,
som om ett skeende och ett varande. Ett skeende och ett varande
vi kan lära oss att aktivt träda in och delta i. Religion är, i
teologen Paul Tillichs ord, "det tillstånd då man känner sig
buren av makten hos tillvaron själv".

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img
  src="/images/2020-05-28-republican-automatons-george-grosz.jpg"
  style="width:90%">
  <figcaption> 

	Är vi endast viljelösa robotar styrda av naturlagarnas
	obevekliga järnhand, eller har vi en meningsfull påverkan på
	vårt liv och våra val? Bild: <i>Republican Automatons</i>
	(1920) av George Grosz.

  </figcaption>
</figure>
