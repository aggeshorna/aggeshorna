---
layout: post
title: Vadan ondska?
---

*Här hittas lite lösa funderingar kring ondskans nödvändighet
som jag skrev ner någon enslig natt i december. Jag vet inte
riktigt själv vad jag ska göra av dem, men kanske kan de vara av
intresse.*

Varför finns ondskan? Varför finns lidandet? Varför är ondskan
nödvändig? Är ondskan nödvändig?

Utan ondska: inget val, ingen frihet, inget ansvar.

Utan ondska: ingen polaritet, ingen dynamik, ingen aktivitet.

Utan ondskan, utan en motpol till det goda och till ljuset, kan
ingenting hända. Utan polariteten, och den energi som bor däri,
finns ingen dynamik, ingen aktivitet och ingen förändring. Och
därmed inte heller någon tid. För vad är tid om inte en följd av
händelser? Om ingenting händer, om ingenting förändras, om allt
är statiskt, hur kan vi säga att tid passerar?

Kanske är ett sätt att förstå evigheten just som en domän där
ingenting händer, där ingenting förändras, där allt är statiskt.
För om någonting händer, om någonting förändras, om någonting är
dynamiskt, hur kan det sägas vara evigt?

Så, utan ondska ingen tid, och utan tid ingen skapelse. För
skapelsen är i tiden, och skapelsen kan inte finnas skild från
tiden.

Men varför denna skapelse? Varför denna dynamik? Varför denna
aktivitet? Varför måste någonting hända? Måste någonting hända?

Kanske, kanske inte. Den frågan är för stor för mig att besvara.
Men saker händer, så mycket vet vi.

För oss som människor, som medvetna varelser, är valet något av
det mest centrala i detta händande. Kanske det mest centrala. Vi
väljer ständigt för vi måste ständigt välja; och valet föder
frihet och frihet föder ansvar -- ansvar att välja "rätt".

Men varför detta väljande? Varför denna frihet? Varför detta
ansvar? På något sätt hör det ihop med domen. Ansvar och dom är
intimt förknippade: inget ansvar, ingen dom.

Domen är ett särskiljande och ett sållande. Skyldig--inte
skyldig. Räddad--inte räddad. Heavenbound--hellbound. Men varför
detta särskiljande? Varför detta sållande?

Jung skriver att människan är den andra skaparen. Det mänskliga
medvetandet ger världen ett objektivt varande, och genom
medvetandet

> bemäktigar hon sig naturen, i det att hon konstaterar världens
> förhandenvaro och i viss mån bekräftar den för skaparen.
> Därigenom blir världen en fenomenvärld, ty utan medveten
> reflexion skulle den inte finnas till. Vore skaparen medveten
> om sig själv, skulle han inte behöva några medvetna skapelser.

Jung menar även att syftet med gudstjänsten, "den tjänst som
människan kan göra Gud", är att

> ljus måtte uppstå ur mörkret, för att Skaparen skall bli
> medveten om sin skapelse och människan om sig själv.
