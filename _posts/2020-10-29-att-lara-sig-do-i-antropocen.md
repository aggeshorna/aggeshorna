---
layout: post
title: Att lära sig dö i antropocen
---

Hur hanterar vi den ofrånkomliga undergången av vår civilisation,
och potentiellt hela mänskligheten, som klimatförändringarna
ställer oss inför? Den frågan ställer Roy Scranton i boken *Att
lära sig dö i antropocen*, och även om man inte fullt ut delar
Scrantons mörka bild av framtiden är frågan viktig. För
undergången kanske inte är ofrånkomlig, men den är fortfarande, i
en eller annan form, fullt möjlig. Jag skulle till och med säga
trolig.

För att besvara sin fråga tar Scranton avstamp i den filosofiska
traditionen om att lära sig dö, en tradition med anor tillbaks
till Sokrates och det gamla Grekland. Att lära sig dö handlar i
grund och botten om att konfrontera sin dödlighet och sina
begränsningar, och utifrån detta möte hitta klokare sätt att
leva. Det handlar, i Scrantons ord, om att "konfrontera det
problematiska med vår situation och se det fulla allvaret i den,
för att sedan hitta den grund utifrån vilken vi förhoppningsvis
kan fortsätta den kamp som är livet". 

Att lära sig dö är något människor sysslat med länge. Det
Scranton menar är nytt med vår nuvarande situation är att vi nu
måste lära oss att dö som civilisation, och inte endast som
individer. Vi måste, som ett kollektiv, lära oss att släppa taget
om våra invanda och för oss självklara sätt att handla och tänka
för att på så sätt "göra oss fria att utan låsningar och fruktan
hantera de problem som nuet ställer oss inför".

För vi kommer inte undan vårt öde. Enligt Scranton är det redan
för sent för att stoppa en okontrollerbar global uppvärmning, och
vår framtid kommer därför att avgöras av vår förmåga att anpassa
oss till denna nya realitet. Men ska det gå vägen får det inte
göras med "panik, harm eller förnekande, utan med tålamod,
besinning och kärlek", och med "ödmjukheten hos dem som vet att
de är dödliga".

Det finns mycket klokt både i Scrantons fråga och i hans
filosofiska utgångspunkt. De klimat- och miljökriser vi befinner
oss i är ett tydligt tecken på att vår nuvarande civilisation,
vårt nuvarande sätt att handla och tänka, inte längre håller. Ska
vi lyckas undvika krisernas värsta konsekvenser -- och lyckas
hantera allt det som redan sker och kommer att ske -- måste vi
lära oss att släppa taget om mycket av det vi har för att få
utrymme till att bygga någonting nytt. 

Men trots Scrantons viktiga fråga och kloka utgångspunkt lyckas
han tyvärr inte komma med särskilt hjälpsamma svar. De stora
utmaningar vi står inför är gemensamma av sin karaktär. Klimat-
och miljökriserna kan inte hanteras av någon enskild, utan vi
måste hantera dem som ett kollektiv. Att ställa om ett samhälle
är ett gemensamt projekt -- och gemensamma projekt kräver
gemensamma berättelser och gemensam grund. Något Scranton inte
ger några egentliga förutsättningar för.

Som läsare lämnar mig Scranton med främst tre saker för att hantera
den katastrof han menar står för dörren: en förkrossande
determinism, en uppmaning att söka individuella meningsprojekt
och värdet av kulturella arkar. Inget av detta utgör en
tillräcklig grund för de gemensamma projekt som krävs.

Scranton hävdar att allt som hänt sedan universums begynnelse
redan låg "i de kort som spelades ut" i den explosion av
kvantenergi som Big Bang innebar. För att just detta ögonblick
skulle vara möjligt måste allt ha hänt precis på det sätt som det
hände. "Inget gick fel", skriver Scranton. "Inga misstag begicks.
Det fanns ingen synd, inga fel, inget fall. Det fanns endast
nödvändighet." 

Och precis som allt som hänt har varit förutbestämt sedan
tidens begynnelse, menar Scranton att även vår eventuella
överlevnad eller undergång redan är bestämd. Allt sker av
nödvändighet; vår möjlighet att påverka är minimal. Som en grund
för att hantera de kriser vi står inför kan jag inte se denna
inställning som annat än förkrossande. 

I förordet till boken citerar Scranton ett mejl han skrivit till
en student. Studenten hade hört honom prata om sin bok och
ställde därför frågan att om allt redan är kört, vilket Scranton
i viss mån påstår, varför ska vi ens bry oss? 

Scranton svarar att precis som vetskapen om vår personliga
förgänglighet och kommande död inte gör att vi ger upp våra liv i
dag, på samma sätt behöver inte vetskapen om vår civilisations
och arts förgänglighet ogiltigförklara de projekt vi företar oss
i våra liv. Men vilka projekt som är värda att engagera sig i kan
inte någon annan avgöra för oss -- det måste vi bestämma själva. 

Scrantons svar är inte felaktigt, det är på många sätt klokt, men
jag finner det ändå otillräckligt. Om allt vi har är individuella
meningsprojekt, hur ska vi lyckas med de gemensamma och
kollektiva ansträngningar som krävs för att undvika, eller
åtminstone hantera, den kommande undergången? De individuella
projekten behövs, men de är inte tillräckliga. Vi behöver också
gemensamma projekt.

Det enda gemensamma projekt som Scranton ger någon grund för
berör bevarandet av vårt kulturella arv. Enligt Scranton
förbinder kulturen det förflutna med framtiden, och de döda med
de levande och de kommande. Kulturen kan ge oss tillgång till
"ett större, kollektivt själv", till "en mänsklig existens som
överskrider varje specifik tid och plats". Detta större,
kollektiva själv har rötter ända tillbaks till mänsklighetens begynnelse, och sträcker sig vidare in i den oroliga framtid som
står för dörren. "Vi är mänskligheten", konstaterar Scranton. "Vi
är de döda. De har blivit vi, liksom vi kommer att bli framtida
generationers döda."

Det arkiv av visdom som vårt kulturella arv utgör är för Scranton
den värdefullaste gåva vi kan ge vidare till framtiden. Han menar
att detta arkiv utgör "inte bara fröbanken för vårt framtida
intellektuella växande, utan också dess jord, dess källa, dess
livmoder". "Om detta att vara människa", skriver Scranton, "ska
betyda något alls i antropocen, om vi ska vägra låta oss sjunka
ner i den fåfänglighet som livet utan minne utgör, så får vi inte
förlora den kunskap som vi under stora vedermödor tillägnat oss
under tusentals år". "Vi får", helt enkelt, "inte ge upp minnet
av de döda."

Här berör Scranton något viktigt. Vårt kulturella arv är
värdefullt, inte minst i tider av kraftig förändring. Det kan ge
oss förankring och en grund att stå på. I vårt förflutna kan vi
även hitta inspiration till andra sätt att leva och vara, både
som individer och som kollektiv. Ska vi lyckas hantera de
omvälvningar vi står inför kommer vi behöva gott om inspiration.

Men även om det Scranton skriver är viktigt är det svårt att inte
samtidigt uppfatta en viss desperation och uppgivenhet. Scranton
uppmanar oss att rädda det som räddas kan, men skriver väldigt
lite om värdet av att samtidigt bygga någonting nytt. Den bördan
lämnar han i stället till de efterkommande. Men framtiden är på
många sätt redan här, vilket inte minst visas av den pågående
pandemin. Att i ett sådant läge nästan helt försaka nybyggandet
för bevarandet är för mig djupt ansvarslöst.

Scranton beskriver väl de kriser vi befinner oss i och de
svårigheter vi står inför, men han lyckas sämre med att ge oss
verktyg för att hantera den dystra framtid som hotar. Hans bok
andas till stor del uppgivenhet, och uppgivenhet är bland det
sista vi behöver.

Men trots all min kritik är Scrantons bok både angelägen och
läsvärd. Inte minst för att den ställer viktiga frågor, och
tvingar till eftertanke. För det är sant som Scranton säger: vår
nuvarande civilisation, som är byggd på och beroende av fossil
energi, är till stor del redan död. Domen har fallit -- frågan är
hur vi hanterar den. Väljer vi att i tid släppa taget och
acceptera att så är fallet, för att på så sätt kunna hitta
klokare vägar framåt, eller fortsätter vi som vanligt tills
kollapsen är ett faktum?

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img
  src="/images/2020-10-29-att-lara-sig-do-i-antropocen.jpg"
  style="width:100%">
  <figcaption>

  	Vy av jorden tagen från Apollo 13 år 1970. Bildkälla: NASA.

  </figcaption>
</figure>
