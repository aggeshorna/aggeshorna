---
layout: post
title: Att möta döden &ndash; och hur det kan lära dig att leva
---

*Detta är den andra delen av tre i en serie om att lära sig att
dö. Följ länkarna för [del ett][del1] och [del tre][del3].*

[del1]: /att-lara-sig-att-do/ "Att lära sig att dö &ndash; en introduktion"
[del3]: /stunder-av-tillracklighet/ "Stunder av tillräcklighet"

En studiekamrat berättade en gång om en bergsklättrare som dött.
Det var en framstående klättrare som en dag klättrat längs en
bergvägg i närheten av sitt hem. Han hade klättrat högt, men
plötsligt tappade han greppet och föll. 

Att falla är vanligtvis inget problem. Han var en van klättrare
och under klättringen hade han monterat säkringar i bergväggen
som skulle fånga upp ett eventuellt fall. Men han föll och första
säkringen brast -- och om första säkringen brister så brister
även resten. 

Säkringarna är anpassade för att fånga vikt från en viss
fallhöjd, och brister första säkringen är fallet för långt för
att övriga säkringar ska hålla. När första säkringen brast fanns
därför ingen tvekan: bergsklättraren föll mot sin död.

I mitt [förra blogginlägg][1] skrev jag om att lära sig att dö.
Jag skrev om att lära sig att dö egentligen handlar om att lära
sig att leva, och jag skrev om att jag först stött på denna tanke
när jag förra sommaren läste några texter av filosofen Seneca.
Tanken intresserade mig, och när jag funderade kring ämnet dök
berättelsen om bergsklättraren upp i minnet. 

[1]: /att-lara-sig-att-do/ "Att lära sig att dö &ndash; en introduktion"

Jag bestämde mig för att göra ett tankeexperiment. Jag ville
undersöka vad jag kunde lära av min egen dödlighet, och därför
placerade jag mig själv i bergsklättrarens skor. 

Om det var jag som klättrade längs med bergväggen, om det var mig
säkringarna inte bar när jag föll; vad skulle jag vilja tänka och
känna när jag såg att första säkringen brast, när jag visste att
jag föll mot min död? Eller annorlunda uttryckt: *hur vill jag
möta döden*?

Jag kom fram till framförallt tre saker:

1. jag vill möta döden med tacksamhet
2. jag vill möta döden utan ånger
3. jag vill möta döden med sorg över det liv jag mister.

Först och främst skulle jag vilja möta döden med tacksamhet: för
det liv jag levt, för de människor jag mött, för all den kärlek
jag fått och gett. Tacksamhet föder glädje, tillfredsställelse
och lugn, och gör den annalkande döden lättare att bära.

För det andra vill jag möta döden utan ånger -- för sådant jag
gjort, såsom misstag jag begått och människor jag sårat, men även
för sådant jag inte gjort: sådant jag önskar att jag
hade gjort, men av någon anledning inte tagit tag i.

Till sist vill jag möta döden med sorg över det liv jag mister,
över allt det liv jag aldrig kommer att få leva. Sorgen är en
bekräftelse av att det liv jag mister är värdefullt, av att det
liv jag inte kommer att få leva är värt att sörja.

<figure style="margin-bottom: 1.5em; margin-top: 2em;">
	<img src="/images/2019-07-03-att-mota-doden.jpg"
	style="width:100%">

	<figcaption>

		Bergsklättring &ndash; ett av otaliga sätt att möta
		döden. 
		
	</figcaption>

</figure>

## Mötet med döden som riktmärke för livet

De tre insikterna formulerade ovan är riktmärken för hur jag vill
möta döden, och likt alla riktmärken är de ett hjälpmedel på
färden, någonting att utgå ifrån när kursen ska sättas. 

När jag väl funnit dessa riktmärken spann jag vidare i tanken.
Vad kan jag lära mig av dem? Vad säger de om hur jag bör leva? Om
färden i fråga är livet självt, hur kan de hjälpa mig att
bestämma färdriktning?


### Att möta döden med tacksamhet

Tacksamhet är i mångt och mycket en färdighet som går att öva.
Genom att ständigt utöva tacksamhet, i stort som smått, kan
tacksamhet bli allt mer av en vana och en grundläggande
inställning till livet. För att kunna möta döden med tacksamhet
är det därför viktigt att leva i tacksamhet till vardags.

Oavsett vart i livet du är, oavsett hur tufft livet ter sig,
finns det alltid någonting att vara tacksam över. Genom att lära
sig att hitta och uppmärksamma det som är värt att tacka för lär
man sig att vara nöjd med och finna glädje i det man har och har
haft; och att släppa ilska och bitterhet över allt det man
mist, allt det man inte har och allt det man aldrig kommer att
få.


### Att möta döden utan ånger 

Ångern går, som jag nämnt ovan, att dela in i två kategorier:
ånger över sådant som gjorts, och ånger över sådant som inte
gjorts.

Lösningen för att undvika ånger för sådant som gjorts är relativt
simpel, åtminstone i teorin: gör inte sådant du kommer, eller
misstänker att du kommer, att ångra. Har väl ett felsteg begåtts
så sök förlåtelse, från dig själv och andra, när tillfälle ges.
Du vet aldrig om fler tillfällen till försoning kommer, och
uppskjuten försoning kan lätt bli utebliven försoning.

Även för ånger över sådant som inte gjorts är lösningen, i
teorin, enkel: gör det du känner starkt för att göra, och gör det
i tid. Livet har en tendens komma i vägen för även de bästa av
planer, och allt som skjuts upp kan lätt förbli ogjort. Låt dig
därför styras främst av det du vill och känner är rätt att göra,
och inte av det du upplever att du borde göra.


### Att möta döden med sorg över det liv jag mister

För att känna sorg över det liv jag mister hjälper det att leva
ett liv som är värt att sörja. Ett liv jag gärna fortsatt få
uppleva. Jag tror att en viktig del i att leva ett sådant liv är
att låta sig vägledas främst av det man vill och upplever är rätt
att göra, inte av det man endast upplever att man borde göra. Att
odla sina intressen, att umgås med människor man uppskattar, att
ständigt lära nytt och utmana sig själv. Att aldrig sluta
fascineras av all den rikedom livet har att erbjuda.

Även här spelar tacksamhet en viktig roll. Genom att lära sig
tacksamhet lär man sig att bättre se det som är vackert, viktigt
och värdefullt i livet -- och att bättre förstå att förlorat liv
är liv värt att sörja.


## Mötet med döden i hinduism och kristendom

I tankeövningen ovan försöker jag bättre förstå hur jag bör leva
genom att bättre förstå hur jag vill möta döden. Det är ett sätt
att med hjälp av döden finna vägar till livet, att lära sig att
leva genom att lära sig att dö. Ett sätt som jag först i
efterhand förstått är långt ifrån originellt. 

Min kunskap om hinduismen är klart begränsad, men jag tycker mig
ändå förstått att just mötet med döden är centralt inom
åtminstone delar av hinduismen. Hur du möter döden avgör, som jag
förstått det, var du hamnar i nästa liv. I Bhagavad-Gita
uttrycker Krishna, som är "the Supreme Personality of Godhead",
saken så här:

> And whoever, at the end of his life, quits his body remembering
> Me alone at once attains My nature. Of this there is no doubt.
> (Bhagavad-Gita 8.5).

Även i Lukasevangeliet går det att ana en liknande tanke. När
Jesus hänger på korset omges han av två förbrytare, varav den ena
tilltalar honom med orden: "Jesus, tänk på mig när du kommer med
ditt rike." Jesus svarar: "Sannerligen, redan i dag skall du vara
med mig i paradiset." 

Alla de brott och alla de misstag förbrytaren begått får i denna
stund en underordnad betydelse. Förbrytaren möter döden med tro,
och det är nog för att bli räddad.

Bibelcitatet ovan visar att en omvändelse på korset kan vara god
nog -- och visar därmed att det aldrig är kört, att hoppet alltid
lever -- men för att kunna möta döden på rätt sätt är det ändå
till hjälp att öva sig medans man lever. Krishna säger:

> With your activities dedicated to Me and your mind and
> intelligence fixed on Me, you will attain Me without doubt.
> (Bhagavad-Gita 8.7).

Även Jesus är tydlig gällande vikten av att leva, och dö, på rätt
sätt. Jesus säger att han är "vägen, sanningen och livet", att
"ingen kommer till Fadern utom genom mig" (Joh 14:6). Han säger
även att den som vill följa denna väg måste "förneka sig själv
och varje dag ta sitt kors och följa mig", för den som "mister
sitt liv för min skull, han skall rädda det" (Luk 9:23--24). 
