---
title: Vad kan jag hoppas på?
layout: post
---

Vad kan jag egentligen hoppas på? Den frågan har följt mig ett
tag nu, inte minst för att omvärlden och världsläget i mångt och
mycket känns så dystert. Utsläppen fortsätter att stiga,
skjutningarna blir allt fler, skövlingen av naturen fortsätter
till synes ohämmat och flykten in i det virtuella likaså. Visst
finns det mottrender och ljusglimtar men de känns ofta så
futtiga, så otillräckliga, i jämförelse. I alltför hög grad
trampar vi endast taktfast vidare längs den dödens stig vi
vandrar, oförmögna att ändra riktning, ovilliga att välja något
annat. Det är lätt att tappa modet.

Men samtidigt: världen är bättre, vackrare, än så. Så fylld av
godhet, skönhet, kärlek att jag vet att alla dystra
framtidsspaningar och domedagsprofetior missar något väsentligt.
Zooma ut och allt blir trender och grafer och utsläppsstatistik
och gängskjutningar som pekar rakt ner i ångesten. Men zooma in,
lyssna, se dig omkring -- och allt blir nåd.

I tillvarons mitt, i dess innersta, finns en kärlek -- en kärlek
som bär allt, genomsyrar allt, skapar och förnyar allt. Jag vet
att den finns där för jag möter den ständigt: i koltrastens
porlande vårsång, i rimfrosten som får snön att gnistra, i
kvällssolens brandröda himmel; i varje leende, varje skratt,
varje omfamning, varje gråt. Ur allt stiger kärleken fram, den
kärlek som är livet självt med all sin bångstyriga, okuvliga
kraft. Jag vet att den finns där även om jag inte alltid förmår
se den, och den berättar för mig, de gånger jag kan och vill
lyssna, att det inte är kört, att det aldrig är kört, att hoppet
alltid lever. Att ljuset, trots allt, är starkare än mörkret.

Och kärleken bär på ett löfte. Inte ett löfte om att allt kommer
vara enkelt, att allt kommer lösa sig och bli som vi önskar --
men ett löfte om att vi inte är ensamma, att vi inte måste bära
allting själva. Eller i orden av han som är livet, sanningen och
kärleken givna kropp: "Jag är med er alla dagar till tidens
slut." Det är ett löfte att ta fasta på, för ljuset lyser i
mörkret och mörkret har inte, och kommer aldrig, att övervinna
det.

Framtiden är oviss, det har den alltid varit och kommer alltid
att vara. Ovissheten hör framtiden till. Vi vet inte hur
framtiden kommer att bli och därför är den öppen, obestämd. Men
samtidigt så är framtiden beroende av vår samtid. Det vi gör idag
skapar ramar och förutsättningar för hur framtiden kan komma att
bli och därför är framtiden också begränsad, förutbestämd. Genom
att tyda samtidens tecken, genom att försöka förstå varifrån vi
kommer och i vilken riktning vi färdas, kan vi ana framtiden, och
den framtid jag anar är på många sätt mörk. Jag anar en framtid
med mer hunger och mer svält, med rasande eldar och svällande
hav, med mer våld, mer krig och ännu fler på flykt. Jag anar allt
högre murar och människor som inte vill, inte kan, inte orkar se.

Men mitt i allt detta finns också kärleken och jag vet att
kärleken vill något annat. Kärleken vill inte svält, vill inte
krig, vill inte våld och förstörelse. Kärleken vill omtanke, vill
gemenskap, vill liv. Jag kallar den kärleken Gud och det är min
tro och övertygelse att Gud bor i oss alla, i allt som är till,
och därifrån, ur vårt och världens inre, viskar, talar, ropar
till oss, manar oss till kärlek och omvändelse, ber oss att
vandra de vägar som leder till liv.

Så, vad kan jag hoppas på? Det bästa svar jag just nu kan ge är
att jag hoppas på den kärlek jag vet finns, den kärlek jag vet är
större än rädslan och hatet och likgiltigheten även om det inte
alltid verkar så. Jag hoppas att vi ska lyssna till den röst som
ropar och ge kärleken plats i vårt och världens liv -- att den
ska få lysa klart och förhoppningsvis allt klarare mitt i all
skit, all smärta och brustenhet.

Så länge kärleken är med är tillvaron inte tom, inte meningslös
-- kärleken fyller den med nåd, med närvaro och mening. Och även
om det ibland kan kännas som sent på jorden, som att loppet redan
är kört, så är det aldrig för sent för kärlekens ord och
handlingar. Dessa ord och handlingar är alltid mål nog i sig
själva, för det är sant så som Václav Havel skrivit: "Hopp är
inte optimism, inte övertygelsen att något kommer att gå bra,
utan vissheten att något har mening utan hänsyn till hur det
kommer att gå."

Jag hoppas och tror även på uppståndelsen -- på livets seger över
döden, på möjligheten till nytt liv. De flesta sår, både
människors och jordens, har en fantastisk förmåga att läka. Och
även om vissa sår är så djupa att de aldrig kan bli riktigt hela
igen så kan de, med tidens hjälp, förhoppningsvis mildras. Jag
hoppas att nytt liv får spira i de liv och på de marker där
förstörelse gått fram.

Kanske kan jag till och med hoppas på en tid då kärleken får lysa
så klart att mörkret endast blir som en skugga. En tid då livets
och kärlekens källa får flöda fritt, en tid då "Gud skall torka
alla tårar", en tid då kärleken så uppenbart är starkare än
skiten så det är som om "ingen sorg och ingen klagan och ingen
smärta skall finnas mer".

Herre, låt det bli så.

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img
	src="/images/2020-02-27-hope-pieter-bruegel-the-elder.jpg"
	style="width:100%">
	<figcaption> 

		<i>Spes (hopp)</i> av Pieter Bruegel den äldre, ca 1560.
		Inskriptionen i bildens nederkant kan översättas med:
		&rdquo;The assurance that hope gives us is most pleasant
		and most essential to an existence amid so many nearly
		insupportable woes.&rdquo;

	</figcaption>
</figure>

