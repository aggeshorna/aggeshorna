---
title: Allt är gåva
layout: post
---

Nyligen var jag en vecka i Lapplandsfjällen, en väldigt fin
vecka. Det är något särskilt med landskapet där uppe. Det är
svårt att inte bli tagen av de öppna vidderna, de storslagna
vyerna, de gamla knotiga björkarna och de höga, snötäckta
topparna. Ställd inför allt detta blev jag ödmjuk inför min egen
litenhet, men också upplyft av att få ta del av och vara mitt i
något så vackert. Det finns en inneboende läkekraft i detta
landskap, att vara där när själen.

Resan blev inte riktigt som jag tänkt mig. Tidigt under veckan
lyckades jag överbelasta mitt vänstra knä -- tung packning,
kraftig stigning och otränad kropp var ingen vinnande kombo --
och jag kunde därför inte gå så mycket som jag tänkt mig.
Istället slog jag läger vid en liten raststuga vid berget
Sjnjierák några kilometer öster om byn Kvikkjokk. Här bodde jag
större delen av veckan och hade gott om utrymme för att vila,
tänka, be och skriva, och läsa Simone Weil som fått följa med på
färden.

En insikt växte i mig under dessa dagar, en insikt om att allt är
gåva. Solen som värmer, vinden som rasslar bland bladen, fjällen
som skiftar i blått vid horisonten -- allt är gåva. Inget av det
har jag förtjänat, det ges mig av nåd. Eller, kanske är det så
att allt inte *är* gåva, men *kan bli* till gåva. 

När jag skalar björken på dess näver, dess hud, för att få
bränsle till min frukosteld kan det nog lika gärna handla om
stöld som gåva. Sliter jag av nävern, kanske mycket mer än jag
behöver, utan någon tanke på björken, så blir nävern till rov och
stöld. Men skalar jag nävern försiktigt, just så mycket som
behövs, samtidigt som jag ber om lov och tackar björken, så kan
nävern bli till gåva. Gåvan blir till inte bara genom givandet,
utan även genom mottagandet.

Men om nu allt är, eller kan bli, gåva, vad betyder det i
faktiskt liv? Om allt är gåva, hur bör jag leva? Den frågan
ställde jag mig där uppe vid Sjnjierák, och här är några tankar
som gavs mig till svar:

<br>
"Ge som gåva vad ni har fått som gåva." Allt jag har är gåva, gåva
jag fått gratis, av nåd, och inte för förtjänst. Liksom jag har
fått för intet bör jag också ge för intet.

<p style="text-align: center;">*</p>

Det som ges mig får jag som gåva -- inte för förtjänst, inte av
rätt, inte som egendom. Allt bör jag därför ta emot så som gåva,
villigt och med ett tacksamt och ödmjukt hjärta.

<p style="text-align: center;">*</p>

Det jag fått som gåva tillhör inte mig -- det är nåd och inte
egendom. Det jag fått som gåva kan också tas ifrån mig. Min roll
är att tacksamt ta emot de gåvor som ges, inte att krampaktigt
klamra fast vid dem så som egendom.

<p style="text-align: center;">*</p>

Det jag får som gåva bör jag ta emot, att neka en gåva är i regel
oförskämt. Gåvor bör tas emot i uppriktig glädje och tacksamhet.

<p style="text-align: center;">*</p>

Om allt är gåva är jag fri från skuld. En gåva, det vill säga en
sann gåva som ges av kärlek och inte av egennytta, är inte en
transaktion och kräver inget i utbyte.

<p style="text-align: center;">*</p>

En gåva är inget jag har rätt till. Om allt är gåva är jag fri
från skuld, men jag är också utan fordringar att driva in. Det
finns ingenting jag har har rätt att kräva. Därför ber vi:
"förlåt oss våra skulder, liksom vi har förlåtit dem som står i
skuld till oss". 

<p style="text-align: center;">*</p>

Om *allt* är gåva -- även bromsarna som äter av min hand,
myggorna som suger mitt blod, värken i mitt svullna och
överbelastade knä -- bör också allt tas emot som gåva, villigt
och i tacksamhet. Men allt är inte gåva. Bromsarna, myggen,
värken i knät kan mycket väl vara det, men missilerna som flyger
mot Odessa är det inte. Vart går gränsen som skiljer gåvan från
det som inte är gåva?

<p style="text-align: center;">*</p>

Återigen: "Ge som gåva vad ni har fått som gåva." En gåva är
ingen transaktion, den kräver inget i utbyte, men den som tar
emot behöver trots det ge. Det finns någonting skevt med en
människa som villigt tar emot men själv aldrig ger. Om allt är
gåva måste jag själv bli en givare.

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img src="/images/2023-07-29-utsikt-vid-sjnjierak.jpg"
	style="width:100%">
	<figcaption> 

		Utsikt över min lägerplats vid Sjnjierák. Om man tittar
		riktigt noga går det att se mitt lilla orangea tält.	

	</figcaption>
</figure>

