---
layout: post
title: Hjärtslag
---

Jag känner mitt hjärta slå  
du-dunk  
du-dunk

Plötsligt vet jag:  
Mina hjärtslag ekar  
i evigheten  
Mitt hjärta och världens  
bultar samman.

<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img
  src="/images/2020-08-12-two-hearts-jim-dine.jpg"
  style="width:95%">
  <figcaption> 

	<i>Two Hearts in a Forest</i> (1981) av Jim Dine.

  </figcaption>
</figure>
