---
layout: post
title: Gratislunchen
---

*Gratislunchen* av Therese Uddenfeldt är en otroligt läsvärd bok.
Väldigt klargörande kring vårt otroliga oljeberoende, och vår
förmåga att glömma vårt välstånds materiella begränsningar.

Uddenfeldt har i boken en liten annan approach till behovet av
omställningen från olja än vad som vanligtvis lyfts. Här är det
inte främst klimatförändringarna som gör att vi behöver ställa
om, utan det faktum att oljan, och därmed tillgången till energi,
är begränsad. Uttaget av olja kommer snart peaka, och när det
händer kommer vi har allt mer begränsade energireserver att
tillgå.

De fossila bränslena tillhör verkligen kärnan av vårt nuvarande
överflödssamhälle. Tidigare samhällen har i stort sett behövt
klara sig på mer eller mindre direkt solenergi, men i och med
upptäckten av kol, och sedan olja och fossil gas, fick vi
plötsligt tillgång till kraftigt koncentrerad solenergi. Som en
följd av detta ökade energitillgången och energiförbrukningen
kraftigt, och vi kunde helt enkelt börja göra och producera
mycket mer.

Utvinningen av fossil energi har ständigt ökat sedan mitten av
1800-talet, och från andra halvan av 1900-talet har utvinningen
riktigt skjutit i höjden. Men energikostnaden för utvinning har
även den ständigt ökat. Man började med att utvinna de
lättåtkomliga tillgångarna först, men i takt med att de
lättåtkomliga källorna sinat har olja och annan fossil energi
börjat utvinnas från allt svåråtkomligare platser och med allt
mer komplicerade metoder.

I boken lyfter Uddenfeldt begreppet Energy Return on Energy
Invested (EROI). Att utvinna energi kräver i sig en viss mängd
energi, och hur förhållandet ser ut mellan spenderad energi i
förhållande till utvunnen energi kan mätas med hjälp av ett
EROI&#8209;värde.

För USA:s tidiga oljeproduktion har EROI&#8209;värdet uppskattats
till ungefär 100:1, man utvann alltså 100 enheter energi per
spenderad energienhet. På 70-talet hade den siffran sjunkit till
ungefär 30:1, och i dag uppskattas EROI&#8209;värdet för
amerikansk olja och gas till ca 10:1. Ju högre EROI&#8209;värde,
desto mer energi finns det över till att göra annat än att
utvinna ytterligare energi. Ju högre EROI&#8209;värde, desto mer
komplexa samhällen är möjliga.

Uddenfeldt lägger även stort fokus vid begreppet *peak oil*. Peak
oil är det tillfälle då oljeproduktionen är som högst, och efter
peak oil inträffat kommer produktionen att börja dala. Detta har
redan hänt i ett antal oljeproducerande länder, men än har det
inte hänt globalt. Många bedömare menar dock att den globala peak
oil närmar sig.

Peak oil handlar inte främst om när oljan tar slut, vilket är en
vanlig missuppfattning, och inte heller så mycket om brist på
olja. I stället handlar det, i Uddenfeldts ord, om *brist på olja
som ger ett stort energiöverskott*. Efter peak oil är nådd kommer
tillgången till olja dala, och den olja som finns kvar kommer bli
allt dyrare och krångligare att komma åt -- EROI&#8209;värdet
kommer helt enkelt att sjunka. Efter peak oil kommer vi vara i
ett läge där "många tävlar om mindre".

Uddenfeldt är även tydlig med att förnyelsebara bränslen inte är
en lösning på denna dalande energitillgång. Till skillnad från
fossil energi består förnybar energi till allra största del av
direkt, i stället för koncentrerad och komprimerad, solenergi. 

För att ersätta de fossila bränslena med förnyelsebart krävs det
därför massiva landarealer. Som exempel lyfter Uddenfeldt att
även om *alla* tillgängliga produkter från världens jordbruk
konverteras till drivmedel skulle det endast täcka 25 procent av
transportsektorns energibehov. För att ersätta den fossila
energin skulle morgondagens förnyelsebara system behöva

> breda ut sig på en yta som är många gånger större än den
> infrastruktur som den ska försörja med energi. Vi skulle svälja
> ännu mer av det som finns kvar av en levande, surrande,
> kryllande biomassa. Kolonisera en ännu större del av biosfären
> för att släcka mänskliga behov och begär.

Att helt ersätta den fossila energin är därför inget rimligt
alternativ. Vi behöver i stället lära oss att skala ned
våra samhällen och nöja oss med mindre. Vi behöver "minska de
energislukande aktiviteterna för att i stället utöva dem som
sätter ett lättare fotavtryck: Förundran. Kärlek, vänskap. Konst,
musik, hantverk."

En sådan övergång kommer inte vara enkel -- vi är allt för fast i
vår bekvämlighet och vårt energiberoende. Det kommer bli
smärtsamt, och troligtvis även blodigt. Och de som kommer lida
värst är, som alltid, världens fattiga och svaga.

Vetskapen om denna kommande förändring ställer, vilket Uddenfeldt
lyfter, ett flertal frågor. Inte minst gäller det frågan om vad
vi vill använda de energireserver, och den koldioxidbudget, vi
har kvar till. Är det till att göra den nödvändiga övergången så
smidig som möjligt, eller till att "i hög takt producera ännu
fler bokhyllor, tröjor och hamburgare"?


<figure style="margin-bottom: 1.5em; margin-top: 2.5em;
text-align:center"> 
  <img src="/images/2020-05-14-gratislunchen.jpg"
  style="width:100%">
  <figcaption> 

	Till vänster: En &rdquo;oil gusher&rdquo; från ett oljefält i
	Texas år 1901. Till höger: Den norska oljeplattformen Troll
	A. En tydlig illustration av hur mycket mer ansträngning som
	krävs för att utvinna olja i dag jämfört med vid oljeålderns
	början.

  </figcaption>
</figure>
