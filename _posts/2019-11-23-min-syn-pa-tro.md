---
layout: post
title: Min syn på tro &ndash; del ett
---

*I ett pågående samtal med en vän kom vi in på temat tro och
religion. Detta är min första text på temat, och det är ett svar
på en [tidigare text][brev-vän] från min vän. På bloggen finns
även [min andra][del-två] och [min tredje][del-tre] text.*

[brev-vän]: /extras/2019-11-23-min-van-om-tro "Min vän om tro &ndash; del ett"
[del-två]: /min-syn-pa-tro-2 "Min syn på tro &ndash; del två"
[del-tre]: /min-syn-pa-tro-3 "Min syn på tro &ndash; del tre"

Så, nu är det min tur att svara.

Till att börja med vill jag konstatera att även om jag inte än
kallar mig kristen, men troligtvis kommer att göra det inom en
inte allt för avlägsen framtid, så verkar våra ingångar till
ämnet tro vara rätt annorlunda. Vilket så klart är spännande.

Själv kommer jag på min pappas sida från en gammal pastorsfamilj.
Min farfarsfar var pingstpastor, och likaså min farfar innan han
och farmor bytte från pingströrelsen till missionskyrkan. 

Mina föräldrar var dock inte i min barndom särskilt troende, min
mamma har aldrig varit troende och min pappa började så smått
återupptäcka sin tro först när jag var i tonåren. Så när jag
växte upp hade jag inte särskilt mycket med religion att göra.

Det började dock ändras till viss del i samband med att jag
upptäckte podden *Myter & Mysterier* under hösten 2014. Den
podden gav, och ger fortfarande, nya perspektiv på inte minst
andliga och mytologiska frågor, och ungefär samtidigt började jag
även läsa mer om framförallt kristendom. Jag började även allt
mer prata om dessa frågor med min far, och våra samtal har varit
viktiga för att forma min syn på tro och religion.

Men för att försöka besvara några av trådarna i ditt meddelande.
Du nämner att religion är metafysik, och det är delvis sant, men
att förstå religion endast inom den ramen blir, så klart, allt
för snävt. Metafysik, åtminstone som jag förstår ordet, handlar
till stor del om en intellektuell konstruktion, och för mig är
religion och Gud någonting väldigt mycket mer än så. 

Som jag förstår religion, grovt uttryckt, så grundar det sig i en
levd erfarenhet, en upplevelse av att möta någonting större och
transcendent -- någonting gudomligt. Religion uppstår i ett
försök att skapa språk, riter, uttryck och berättelser för att
förstå, förmedla och hantera sådana erfarenheter. 

Metafysiken och de intellektuella konstruktionerna finns
onekligen där, men de är -- eller bör åtminstone vara --
sekundära till den levda erfarenheten. Alekséj Karamazov, en utav
bröderna Karamazov, uttrycker det klokt när han säger att
"kärleken måste gå före logiken", och hävdar att "det är först då
som man också fattar meningen".

Du har rätt i att Guds existens inte går att bevisa. Men för mig
är bevis för den typen av frågor överhuvudtaget rätt
ointressanta. Jag håller med pastorn John Ames, en av
karaktärerna i Marilynne Robinsons fantastiska romanserie
*Gilead*, när han om bevis ger rådet "bry dig överhuvudtaget inte
om dem". För, som han säger, 

> de är aldrig tillräckliga i förhållande till frågan, och jag
> tycker alltid att de är en smula ovidkommande, för de söker åt
> Gud en plats som är inom räckhåll för vårt medvetande.

Gud är större, som ärkebiskop Antje Jackeléns valspråk lyder, och
försöker man förminska Gud till någonting som går att bevisa
kommer man ofelaktligen att gå vilse. För att återigen citera
John Ames:

> Man kan bedyra existensen hos något -- Varat -- utan att ha den
> ringaste aning om vad det är. Och Gud är på en helt annan skala
> -- om Gud är Existensens upphov, vad kan det då betyda om man
> säger att Gud existerar?

Har man, liksom jag, uppfattningen att Gud är någonting som
främst måste erfaras i stället för att greppas intellektuellt,
blir de uppfattningar du uttrycker om att Gud har fullt upp med
att rodda universum och därför inte har tid att bry sig om våra
vardagsliv, och att han är såpass väsensskild från oss att han
inte kan begripa oss, smått irrelevanta. 

Den typen av funderingar tyder på en intellektualiserad gudssyn
där logiken går före kärleken. Där Gud blir något form av logiskt
problem som med logik bör lösas. Det menar jag är fel sätt att
närma sig Gud.

Det betyder inte att vår intellektuella förmåga och vårt kritiska
tänkande bör förkastas när det kommer till Gud, tvärtom. Vår
förmåga till kritiskt tänkande är en stor gåva, och använder vi
inte den gåvan kommer vi att gå vilse. 

Men vi behöver inse vårt intellekts begränsningar och våga
erkänna värdet hos de erfarenheter, upplevelser och känslor som
vittnar om det gudomliga. Vårt intellekt -- och även tradition
och dogmatik -- behövs för att vi ska förstå och kunna använda
sådana erfarenheter, upplevelser och känslor på ett klokt och
kärleksfullt sätt. Men intellektet och logiken är sekundära, det
är erfarenheterna, upplevelserna och känslorna -- kärleken -- som
behöver vara grunden.

Vill vi närma oss Gud behöver vi även erkänna och inse vår egen
otillräcklighet och vårt beroende, för så länge som vi är
uppfyllda av vår egen förträfflighet kommer vi vara stängda för
Guds, och även andra människors, kärlek. Detta är något som jag
själv har kämpat och fortfarande kämpar med.

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img
	src="/images/2019-11-23-faith-pieter-bruegel-the-elder.jpg"
	style="width:100%">
	<figcaption> 

		<i>Fides (tro)</i> av Pieter Bruegel den äldre, ca 1560.
		Inskriptionen i bildens nederkant kan översättas med:
		&rdquo;Above all we must keep faith, particularly in
		respect to religion, for God comes before all, and is
		mightier than man.&rdquo;

	</figcaption>
</figure>
