---
layout: post
title: Domen tynger oss
---

Domen tynger oss  
Domaren skrämmer oss  
Men kärleken kräver rättvisan  
och rättvisan kräver Lagen  

Så görs Älskaren till domare:  
kärleken kräver Lagen  
kärleken är Lagen  
och Lagen kräver sin domare  

<figure style="margin-bottom: 1.5em; margin-top: 2em;
text-align:center"> 
	<img
	src="/images/2019-12-23-judgement-day-aaron-douglas.jpg"
	style="width:85%">
	<figcaption> 

		<i>The Judgement Day</i> av Aaron Douglas.

	</figcaption>
</figure>
