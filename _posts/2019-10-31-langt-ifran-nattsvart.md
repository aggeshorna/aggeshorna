---
layout: post
title: Långt ifrån nattsvart för klimatet
---

*Denna text är en debattartikel som tidigare publicerats i VLT,
både [på nätet][vlt] och i tryck.*

[vlt]: https://www.vlt.se/artikel/debatt-langt-ifran-nattsvart-for-klimatet

Ulf Hedman har i en debattartikel undrat varför vi ska fortsätta
kämpa för klimatet. Han menar att den verklighet forskningen
pekar på är såpass dyster att vi lika gärna kan konstatera att
det är kört och ge upp.

Tyvärr har Ulf till stor del rätt -- forskningen är dyster. För
att alls ha någon rimlig chans att begränsa uppvärmningen till
1,5 grader, vilket krävs för att undvika de värsta
klimatkatastroferna, behöver vi halvera våra utsläpp till runt år
2030, och nå nollutsläpp runt år 2050.

Det är en gigantisk uppgift. Det kommer krävas genomgripande
förändringar i samhällets alla delar och nivåer, och trots
brådskan har den egentliga omställningen ännu knappt påbörjats.
Kurvorna pekar fortfarande uppåt, och utsläppen fortsätter att
öka. Utmaningen kan synas oöverkomlig, och Ulfs fråga är
berättigad. Om det ser så mörkt ut, varför ens försöka?

Men även om det börjat skymma är det ännu långt ifrån nattsvart.
IPCC, den tyngsta aktören inom klimatforskningen, har konstaterat
att 1,5-gradersmålet fortfarande är möjligt att nå, och gett en
överblick på hur det skulle kunna gå till.

Och det finns många ljuspunkter i mörkret -- inte minst de
klimatstrejker som sker världen över. Över sju miljoner människor
deltog i de senaste globala strejkerna, och i det bor en väldig
kraft. Lyckas vi ta till vara på den kraften kan vi uträtta
mycket.

Så även om svaret på om vi kan undvika en klimatkatastrof skulle
vara ett "kanske inte", eller till och med ett "troligtvis inte",
är det just detta "kanske", detta "troligtvis", vi behöver ta
fasta på. Dessa ord förmedlar en tvekan, och i denna tvekan bor
ett hopp -- möjligheten att det ändå kan gå.

Men det är en ömtålig möjlighet. En möjlighet som kräver att vi
erkänner den och agerar därefter för att den ska fortsätta leva.
Om vi i stället förnekar möjligheten och lever som om allt är
kört, ja, då är det just kört det är. Möjligheten ställer därför
krav, och den bär på ansvar.

Som människor bär vi alla på ansvar: för oss själva, för
varandra, för dem som kommer efter oss, för allt det liv och för
hela den värld som omger oss och som är själva grunden för vår
existens. Vart detta ansvar kommer ifrån vet jag inte -- men att
det finns, att vi alla bär på det och att det tillhör själva
kärnan av vad det är att vara människa är jag övertygad om.

De miljökriser vi står inför berör detta ansvar i grunden. De
hotar i stort sett hela tillvaron som vi känner den, och alla är
vi mer eller mindre delaktiga i att orsaka dem. Därför har vi
också alla ett ansvar för att bekämpa dem. I vilken mån det
ansvaret berör just dig kan bara du själv avgöra, men att helt
abdikera från det kan aldrig vara rätt svar. Att ge upp medans
möjligheten lever är just detta: att abdikera sitt ansvar.

Dessutom, har vi något vettigt alternativ till att verkligen
försöka? Om vi inte försöker, om vi ger upp, dömer vi hundratals
miljoner människor, om inte fler, till fattigdom, nöd och död. Vi
tillåter den pågående massutrotningen av djur och växter att
fortsätta, och vi ger upp drömmen om en framtid att längta till,
drömmen om ett samhälle som är bättre än det vi har.

Den vägen vägrar jag, och många med mig, att välja. Så länge
klimatkatastrofen ännu är möjlig att förhindra kommer jag därför
fortsätta kämpa för att göra den möjligheten till verklighet.

<p style="margin-top:1cm;">
	<b>August Lindberg</b><br>
	Politisk sekreterare för MP Västerås
</p>

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img src="/images/2019-10-31-langt-ifran-nattsvart.jpg"
	style="width:100%">
</figure>
