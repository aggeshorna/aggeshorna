---
layout: post
title: Evig tillväxt är inte hållbart
---

*Denna text är en debattartikel som tidigare publicerats i
[VLT][vlt]. På deras hemsida finns även ett kort svar från
ledarskribenten Joakim Broman.*

[vlt]: https://www.vlt.se/artikel/debatt-evig-tillvaxt-ar-inte-hallbart

I [en ledare i VLT][ledare] hävdar Joakim Broman att ekonomisk
tillväxt är nödvändigt för att hantera den pågående klimatkrisen.
Han presenterar detta som en entydig sanning från den ekonomiska
forskningen och skriver att påståenden om att tillväxt står i
strid med hållbar utveckling är "grundfalska". Tyvärr så gör
Broman det lite väl lätt för sig.

[ledare]: https://www.vlt.se/artikel/ledare-illa-att-thunberg-inte-tar-till-sig-ekonomisk-vetenskap

Ekonomisk tillväxt har länge varit förknippat med både ökade
koldioxidutsläpp och ökad resursförbrukning. För att ekonomisk
tillväxt ska vara hållbar behöver den därför frikopplas
(engelskans "decouple") från sin negativa miljöpåverkan.

Frikopplingen behöver dessutom ske i så pass hög grad att
resursförbrukningen och miljöförstöringen inte bara slutar öka,
utan minskar kraftigt. Bland annat behöver de globala
koldioxidutsläppen mer än halveras till 2030 för att
uppvärmningen med en rimlig chans ska kunna begränsas till 1,5
grader.

Förra året släpptes rapporten [*Decoupling debunked*][debunk] i
vilken författarna undersöker sannolikheten för en sådan
frikoppling. Rapporten baseras på över 200 studier och rapporter,
och slutsatserna är tyvärr rätt dystra.

[debunk]: https://eeb.org/library/decoupling-debunked/

Författarna konstaterar att det inte "finns några empiriska
bevis" för att någon egentlig frikoppling sker. Dessutom listar
de ett antal anledningar till varför det är osannolikt, "om inte
direkt orealistiskt", att en tillräcklig frikoppling kommer ske i
framtiden.

Broman lyfter USA och Sveriges ekonomier som goda exempel på
frikoppling. Han skriver att Sveriges ekonomi växte med 150
procent mellan 1970 och 1990 samtidigt som koldioxidutsläppen
halverades, och att USA:s ekonomi växte med 250 procent mellan
1970 och 2015 samtidigt som energianvändningen och utsläppen
planade ut.

Ett problem med dessa siffror är att de endast visar utsläpp som
skett inom landets gränser, och tar därför inte hänsyn till
utsläpp från importerade varor eller flygresor till utlandet.

Sedan 70-talet har rika länder flyttat mycket av sina utsläpp
till fattigare länder. I stället för att själva producera
resurskrävande och utsläppstunga varor importerar vi dem.
Prylarna förbrukas i USA eller Sverige, men utsläppen sker i
Indien eller Kina. I Decoupling debunked skriver författarna att
denna utsläppsflytt är tillräckligt stor för att på egen hand
förklara de påstådda fall av frikoppling som lyfts i de studier
de undersökt. 

Tillväxt är alltså betydligt mer problematiskt än Broman påstår.
För egen del blir jag allt mer övertygad om att tillväxt är något
vi borde lägga av med, åtminstone i världens rikare länder. 

Men för att det ska vara möjligt behöver vi släppa vårt ofta
ensidiga fokus på tillväxt och i stället lyfta frågor som i dag
är eftersatta i det offentliga samtalet. Inte minst gäller det
frågan om vad som faktiskt krävs för att vi ska kunna leva
fullvärdiga och rika liv -- och hur vi skapar långsiktiga
förutsättningar för detta även om ekonomin inte växer.

<p style="margin-top:1cm;">
	<b>August Lindberg</b><br>
	Politisk sekreterare för MP Västerås
</p>

<figure style="margin-bottom: 1.5em; margin-top: 2em;
text-align:center"> 
	<img src="/images/2020-01-27-greta-thunberg.jpg"
	style="width:100%">
	<figcaption> 

		Greta Thunberg har flera gånger pekat ut den ekonomiska
		tillväxten som ett problem &ndash; något som
		ledarskribenten Joakim Broman reagerat på.

	</figcaption>
</figure>
