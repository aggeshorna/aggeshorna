---
layout: post
title: Betraktelser från ett blåbärssnår
---

Bland blåbärsrisen sitter jag, strax intill stigen där människor
passerar förbi. Joggandes. Raskt gåendes. Hundrastandes.
Motionerandes. Men sällan strosandes. Jag tittar upp när någon
passerar. Jag söker deras blickar, men får inget svar. 

Ser de mig här bland snåren? Eller är de alltför upptagna med sin
Uppgift, sitt Syfte här i skogen, för att se sin omgivning?
Blickarna stadigt fästa på motionsspåret vid deras fötter, öronen
igenbommade av hörlurar. Eller märker de mig kanske, men väljer
att ignorera mig?

För ett tag sedan var jag, för första gången på allt för länge, i
skogen och bara strosade. Inget motionerande, inget bärplockande,
inget lyssnande i hörlurar -- utan endast strosande. Lugnt
promenerandes med min uppmärksamhet spelandes över synerna,
ljuden, tankarna lät jag mig själv bara vara.

Jag kände mig som en rebell, en upprorsmakare. Känslan kan ha
stärkts av mina bara fötter, men detta att lugnt strosa genom
skogen kändes som en närmast civilisationskritisk handling.

Jag var nämligen ensam i mitt strosande, men inte ensam i skogen.
Skogen var fylld av hurtbullar i grälla träningskläder, med
mobilen i hand och hörlurarna på plats. Det gjorde mig något
nedstämd.

Jag har inget emot att människor motionerar, men skogen kan vara
så mycket mer än endast en plats för hurtigt motionerande eller
hundrastande. Inte minst kan skogen vara en plats för meditation
och reflektion -- en plats för nya insikter.

Skogen kan tala till oss om annat än stadens buller, reklamens
tomma begär och livspusslets jäkt. Skogen kan ge en skymt av ett
annat sätt att vara. Även de rätt sorgliga skogarna vi oftast
vistas i -- produktionsskogarna, motionsskogarna, stadsskogarna,
långt ifrån de mer orörda skogarnas trollska majestät -- har den
förmågan. 

Men för att höra, måste vi lyssna. Vi måste släppa på kontrollen.
Vi måste lägga undan vår Uppgift, vårt Syfte, och lämna över
tyglarna. Vi måste ge skogen -- världen -- ordet, och låta oss
själva bara vara.

<figure style="margin-bottom: 1.5em; margin-top: 2em;"> 
	<img src="/images/2019-10-08-blueberries.jpg"
	style="width:100%">
	<figcaption> 

		Bild av <a
		href="https://pixabay.com/sv/users/Kjerstin_Michaela-2086286/">Kjerstin
		Michaela Haraldsen</a>.


	</figcaption>
</figure>
