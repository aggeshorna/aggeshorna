---
layout: post
title: Att lära sig att dö &ndash; en introduktion
---

*Detta är den första delen av tre i en serie om att lära sig att
dö. Följ länkarna för [del två][del2] och [del tre][del3].*

[del2]: /att-mota-doden/ "Att möta döden &ndash; och hur det kan lära dig att leva"
[del3]: /stunder-av-tillracklighet/ "Stunder av tillräcklighet"

Förra sommaren läste jag några av den romerska filosofen Senecas
texter. Det var intressant läsning, Seneca säger mycket klokt,
men det var särskilt ett tema jag fastnade för: Senecas uppmaning
om att lära sig att dö.

> Ingenting är mindre kännetecknande för en upptagen man än att
> verkligen leva; ingen kunskap är svårare än just denna ... att
> leva måste man lära sig under hela livet och, vilket du kanske
> kommer att mera förundra dig över, under hela livet måste man
> lära sig att dö.

I tidigare blogginlägg har jag skrivit om döden i olika
skepnader: om döden i [avslutets och förnyelsens
skepnad][avslut], om [döden som mjölnare][mjölnare]. Här
framträder döden i ännu en skepnad: som vägvisare, som lärare i
levnadens konst, som mentor.

[avslut]: /tre-insikter-om-livet-och-doden/	"Tre insikter om livet och döden"
[mjölnare]: /doden-som-mjolnare/ "Döden som mjölnare"

Att lära sig att dö handlar främst om att lära sig att leva. Dö
ska vi alla, och döden kan ofta vara svår att bära. Den är inte
sällan skräckinjagande, och därför lockande att fly från. Men
flyr man från döden går man miste om något väsentligt. 

Att fundera kring döden kan nämligen, på ett smått paradoxalt
vis, vara mer livsbejakande än många verkar tro. Döden kan ge
livet vikt, mening och substans. Döden ger livet en kontext och
ett sammanhang, och genom att fundera kring döden kan vi lära oss
om vad det innebär att leva.

Det bör nämnas att reflektionerna i denna text främst berör
vår egen personliga död. Men döden har även en annan, ytterst
relevant, aspekt: andras död. 

> It is the death of those we are bound to in love that undo us,
> that unstitch our carefully tailored suit of the self, that
> unmake whatever meaning we have made ... The entire difficulty
> here is imagining what sort of contentment or tranquillity
> might be possible in relation to the deaths of those we love.

Orden ovan tillhör Simon Critchley, vars bok *The book of dead
philosophers* har varit en stor hjälp i skapandet av denna text.
Även om Critchley har rätt i att andras död är minst lika viktig
som vår egen har jag för denna text, och kommande texter på samma
tema, valt ett annat fokus: hur man genom förhållandet till sin
egen död kan finna vägar som leder till livet.

<hr class="textSeparator">

"To philosophize is to learn how to die." Dessa ord tillhör den
romerska filosofen Cicero, men många andra har uttryckt samma
tanke. Ända sedan Sokrates, den västerländska filosofins fader,
har förhållandet till döden varit central inom filosofin.

Enligt Epikuros finns inget skräckinjagande i livet "for one who
has grasped that there is nothing fearful in the absence of
life", och Seneca hävdar att "he will live badly who does not
know how to die well". Spinoza skriver: "A free man thinks of
nothing less than death, and his wisdom is a meditation on life,
not on death", och Montaigne har sagt att om han skulle skriva en
bok (Montaigne var mer förtjust i essäer än böcker) skulle han
skriva en förteckning över hur människor dött, eftersom "he who
would teach men to die would teach them to live". 

Även för 1900-tals filosofen Heidegger var förhållandet till
döden central. Som människor är vi ändliga, och för att kunna
leva autentiskt, enligt Heidegger, måste vi konfrontera vår
ändlighet, vår dödlighet, och skapa en mening utifrån den -- det
Heidegger kallar en "till-döden-varo". 

Inom många religioner är en medvetenhet om döden ofta viktig, till
och med nödvändig, för ett gott och riktigt leverne. Det gäller
inte minst för kristendomen. Inom klassisk kristendom är den
jordiska döden en port som leder till evigheten. Hur vi spenderar
vår tid på jorden avgör om denna port leder till evig salighet i
himmelrikets salar, eller evig pina i helvetets flammor. Genom
att minnas vår dödlighet blir vi påminda om vad som inte besitter
annat än ett falskt värde som endast gäller i detta liv -- såsom
rikedomar, makt och sinnliga njutningar -- och vad som är på
allvar värdefullt, och som fortsätter ha värde även efter detta
liv är slut -- såsom kärlek, trofasthet och sanning. I Jesus
Syraks vishet, en av det gamla testamentets apokryfer, uttrycks
det så här:

> Hon bär på agg, hon som själv är dödlig -- vem skall då ge
> försoning för hennes synder? Tänk på livets slut och hata inte
> mer; minns döden och förgängelsen, håll fast vid buden. (Syr
> 28:5--6).

<hr class="textSeparator">

Allt som existerar är begränsat: i tid, i rum och i kapacitet.
Det gäller inte minst oss människor; något vi behöver acceptera
om inte livet ska bli än svårare att leva. Vi behöver, i Simon
Critchleys ord, "accept our dependence and limitedness in a way
that does not result in disaffection or despair. It is rather the
condition for courage and endurance."

Att acceptera sina begränsningar innebär inte att kapitulera
inför dem; det är genom att utmana våra begränsningar vi växer
som människor. Men alla begränsningar är inte lika lämpliga att
utmana. Vi måste först förstå och acceptera våra, och tillvarons,
begränsningar för att se vart möjligheter för utmanande och
utveckling finns.

Epikuros, och många med honom, hävdar att döden för oss är ett
intet. "When we exist death is not yet present, and when death is
present, then we do not exist." Genom att inse att döden är ett
intet kan vår dödlighet, enligt Epikuros, bli "a matter for
contentment, not by adding a limitless time to life but by
removing the longing for immortality". 

Även om jag inte helt delar Epikuros syn på döden som ett intet
tror jag att det är en klok inställning till många av livets
begränsningar att låta dem förbli just "a matter for
contentment". Att finna tillfredsställelse med det vi inte kan
påverka, och istället rikta vår energi och tid mot de uppsjö
möjligheter som trots allt är oss givna.

Döden är en av alla de begränsningar som är omöjliga att
övervinna. I bästa fall går döden att skjuta upp, men den går
aldrig att undfly. Vi behöver istället lära oss att leva med vår
dödlighet; att släppa vår skräck inför den. Lära oss att, i
Marcus Aurelius ord, "dö med glatt sinne, liksom den mogna oliven
faller, i det den välsignar trädet, som skapat den, och tackar
grenen som burit den." 

Just detta att släppa sin skräck inför döden är en central del i
att lära sig att dö. Döden är omöjlig att undvika och kan slå
till när helst den behagar. En fruktan för döden är därför, som
Montaigne skriver, "a perpetual torment, for which there is no
consolation"; denna fruktan förvägrar oss "a pure and pleasant
taste of living, without which all other pleasure would be
extinct". Men lär vi oss att släppa vår skräck inför döden -- lär
vi oss att möta döden och säga: "Jag räds dig inte" -- kan vi i
stället finna en del av det mod som krävs för att på allvar våga
leva.

<figure style="margin-bottom: 1.5em; margin-top: 2em;">
	<img src="/images/2019-06-08-att-lara-sig-att-do.jpg" style="width:100%">
	<figcaption> 
	
		En av de väldiga gravepitafierna i Västerås domkyrka. Den
		utmärglade figuren i förgrunden höll ursprungligen i en
		banderoll med texten <i>Memento mori</i> &ndash; minns
		att du ska dö.

	</figcaption>
</figure>
