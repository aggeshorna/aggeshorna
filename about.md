---
layout: page
title: Om mig
permalink: /ommig/
---

Namnet mitt är August Lindberg, och detta är mitt alldeles egna
hörn av cyberrymden. Här kommer jag lägga upp funderingar och
reflektioner kring ämnen som intresserar mig, till min egen och
förhoppningsvis andras förnöjelse.

Denna hemsida är byggd med publiceringsplattformen Jekyll. Koden
till hemsidan går att hitta på [GitLab][gitlab].

[gitlab]: https://gitlab.com/aggeshorna/aggeshorna


## Kontakt

E-post: [aggeshorna@gmail.com](mailto:aggeshorna@gmail.com).
